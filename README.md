# Land

懒得写代码

衍生版本:

land的SpringBoot 2 的版本: 

land-spring-boot-2

https://gitee.com/iNick/land-spring-boot-2

land的SpringBoot 3 的版本:

land-spring-boot-3

https://gitee.com/iNick/land-spring-boot-3

land的代码生成器

https://gitee.com/iNick/land-code-generator

## 模块说明

整个模块相对比较简单

**code-generator**: 代码生成工具,里面的模板进行了魔改

**common-core**: 整套项目的核心模块,一些公共配置和工具在里面

**land-cms**: 后台管理系统API服务

**land-web**: 前端H5的API服务


## 代码习惯

### 数据库

- 数据库主键使用数据库名+id, 例如: system_user的id为system_user_id
- 表名和字段使用下划线命名, 例如: system_user.system_user_id
- 数据库必须要有的字段:
    - xxx_id: 主键, 类型为bigint unsigned, 长度为20位
    - create_time: 创建时间, 类型为datetime
    - update_time: 更新时间, 类型为datetime
    - deleted: 是否删除, 类型为tinyint unsigned, 长度为1位
- 业务数据推荐包含:
    - version: 版本号, 类型为bigint unsigned, 长度为20位

### 代码

- 对象拷贝采用 BeanCopier 封装之后的  BeanCopierUtil
- 依赖注入方式采用尽量采用 构造函数注入, 搭配 @RequiredArgsConstructor 进行使用
- 类名使用驼峰命名, 例如: SystemUser
- 方法名使用驼峰命名, 例如: getSystemUserById
- 方法名尽量使用动词, 例如: getSystemUserById
- 小项目中没有使用Impl实现Service层,直接在Service层实现方法
- 使用的类和对象命名:
    - Query: 查询类使用Query结尾, 例如: SystemUserQuery
    - DTO: 数据传输对象使用DTO结尾, 例如: SystemUserDTO
    - VO: 视图对象使用VO结尾, 例如: SystemUserVO
    - BO: 业务对象使用BO结尾, 例如: SystemUserBO
    - Export: 导出对象使用Export结尾, 例如: SystemUserExport
    - Controller: 控制层对象使用Controller结尾, 例如: SystemUserController
    - Service: 业务接口使用Service结尾, 例如: SystemUserService
    - Impl:  业务逻辑实现类使用Impl结尾, 例如: SystemUserServiceImpl
    - Manager: 管理对象使用Manager结尾, 例如: SystemUserManager
    - Mapper: 数据库操作类使用Mapper结尾, 例如: SystemUserMapper

### 目录结构

基本结构如下

```
- src
  - main
    - java
      - xin.nick
        - config
        - controller
        - service
        - manager  
        - mapper
        - entity
        - domain
          - query
          - dto
          - vo
          - bo
          - export
        - util
        
```

## 注意事项

**接口响应数据**

注意看 controller 里面的方法返回都是没有加 Result 统一返回体的

因为所有的统一返回使用了 拦截器 进行处理,具体代码在 [UnifiedResponse.java](common%2Fcore%2Fsrc%2Fmain%2Fjava%2Fxin%2Fnick%2Fcommon%2Fcore%2Finterceptor%2FUnifiedResponse.java)

所以需要统一返回格式的接口,加上 [@UnifiedResult](common%2Fcore%2Fsrc%2Fmain%2Fjava%2Fxin%2Fnick%2Fcommon%2Fcore%2Fannotation%2FUnifiedResult.java) 注解就可以

**登录接口**

在项目里面是没有 LoginController 逻辑的,
只是写了一个没有逻辑的 [DefaultController.java](land-cms%2Fsrc%2Fmain%2Fjava%2Fxin%2Fnick%2Fsystem%2Fcontroller%2FDefaultController.java)用于Swagger文档展示

因为项目的登录和退出登录都是通过 Spring Security 的过滤器来实现的

Spring Security默认的 formLogin 方式默认是不支持POST的body传参的

所以在项目通过继承 UsernamePasswordAuthenticationFilter 实现了 [JsonLoginFilter.java](land-cms%2Fsrc%2Fmain%2Fjava%2Fxin%2Fnick%2Fsystem%2Ffilter%2FJsonLoginFilter.java)
用于接收JSON格式的参数进行登录



## 其他

代码提交说明规范

``` text
feat： 新增 feature
fix: 修复 bug
docs: 仅仅修改了文档，比如 README, CHANGELOG, CONTRIBUTE等等
style: 仅仅修改了空格、格式缩进、逗号等等，不改变代码逻辑
refactor: 代码重构，没有加新功能或者修复 bug
perf: 优化相关，比如提升性能、体验
test: 测试用例，包括单元测试、集成测试等
chore: 改变构建流程、或者增加依赖库、工具等
revert: 回滚到上一个版本
init: 初始化
build: 构建项目
scope: commit 影响的范围, 比如: route, component, utils, build…
subject: commit 的概述
```
