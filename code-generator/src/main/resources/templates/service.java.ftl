package ${package.Service};

<#assign thisName="${table.comment!}"/>
<#assign thisEntity="${entity?uncap_first}"/>
<#assign thisDTO="${entity}DTO"/>
<#assign thisQuery="${entity}Query"/>
<#assign thisVO="${entity}VO"/>
import ${package.Entity}.${entity};
import ${superServiceClassPackage};
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${package.Parent}.domain.dto.${entity}UpdateDTO;
import ${package.Parent}.domain.dto.${entity}CreateDTO;
import ${package.Parent}.domain.query.${thisQuery};
import ${package.Parent}.domain.vo.${thisVO};

import java.util.List;

/**
 * <p>
 * ${thisName} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {


    /**
     * 获取${thisName}详情
     * @param id
     * @return
     */
    ${thisVO} getDetail(Long id);

    /**
     * 获取${thisName}列表
     * @param query
     * @return
     */
    List<${thisVO}> getList(${thisQuery} query);

    /**
     * 获取${thisName}列表分页
     * @param query
     * @return
     */
    Page<${thisVO}> getListPage(${thisQuery} query);

    /**
     * ${thisName}添加
     * @param dto
     * @return
     */
    ${thisVO} add${entity}(${entity}CreateDTO dto);

    /**
     * ${thisName}更新
     * @param dto
     * @return
     */
    ${thisVO} update${entity}(${entity}UpdateDTO dto);

    /**
     * ${thisName}删除
     * @param id
     * @return
     */
    void deleteById(Long id);

    /**
     * ${thisName}根据id批量删除
     * @param idList
     * @return
     */
    void deleteByIdList(List<Long> idList);

}
</#if>
