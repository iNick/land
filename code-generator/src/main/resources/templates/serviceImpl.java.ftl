package ${package.ServiceImpl};

<#assign thisName="${table.comment!}"/>
<#assign thisEntity="${entity?uncap_first}"/>
<#assign thisDTO="${entity}DTO"/>
<#assign thisQuery="${entity}Query"/>
<#assign thisVO="${entity}VO"/>
<#list table.fields as field>
    <#if field.keyFlag>
        <#assign keyPropertyName="${field.propertyName}"/>
    </#if>
</#list>

<#if keyPropertyName??>
    <#assign thisEntityId="${keyPropertyName?cap_first}"/>
<#else>
    <#assign keyPropertyName="id"/>
    <#assign thisEntityId="Id"/>
</#if>
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${superServiceImplClassPackage};
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${package.Parent}.domain.dto.${entity}UpdateDTO;
import ${package.Parent}.domain.dto.${entity}CreateDTO;
import ${package.Parent}.domain.query.${thisQuery};
import ${package.Parent}.domain.vo.${thisVO};
import xin.nick.common.core.util.BeanCopierUtil;
import xin.nick.common.core.constant.SystemConstants;

import java.util.List;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> {


    /**
     * 获取${thisName}详情
     * @param ${keyPropertyName} Long
     * @return ${thisVO}
     */
    public ${thisVO} getDetail(Long ${keyPropertyName}) {
        Assert.notNull(${keyPropertyName}, SystemConstants.VALUE_IS_NOT_NULL_MSG_TEMPLATE, "${keyPropertyName}");
        ${entity} ${thisEntity} = getById(${keyPropertyName});
        Assert.notNull(${thisEntity}, "${thisName}不存在");
        return BeanCopierUtil.copyProperties(${thisEntity}, ${thisVO}.class);
    }

    /**
     * 获取${thisName}列表
     * @param query ${thisQuery}
     * @return List<${thisVO}>
     */
    public List<${thisVO}> getList(${thisQuery} query) {

        LambdaQueryWrapper<${entity}> queryWrapper = getListQueryWrapper(query);

        List<${entity}> ${thisEntity}List = list(queryWrapper);

        return BeanCopierUtil.copyToList(${thisEntity}List, ${thisVO}.class);

    }

    /**
     * 获取${thisName}列表分页
     * @param query ${thisQuery}
     * @return Page<${thisVO}>
     */
    public Page<${thisVO}> getListPage(${thisQuery} query) {

        LambdaQueryWrapper<${entity}> queryWrapper = getListQueryWrapper(query);

        Page<${entity}> page = page(new Page<>(query.getCurrent(), query.getSize()), queryWrapper);

        return (Page<${thisVO}>) page.convert(entity -> BeanCopierUtil.copyProperties(entity, ${thisVO}.class));
    }

    /**
     * ${thisName}添加
     * @param dto ${entity}CreateDTO
     * @return ${thisVO}
     */
    public ${thisVO} create${entity}(${entity}CreateDTO dto) {
        ${entity} ${thisEntity} = BeanCopierUtil.copyProperties(dto, ${entity}.class);
        ${thisEntity}.set${thisEntityId}(null);
        save(${thisEntity});
        return getDetail(${thisEntity}.get${thisEntityId}());
    }

    /**
     * ${thisName}更新
     * @param dto ${entity}UpdateDTO
     * @return ${thisVO}
     */
    public ${thisVO} update${entity}(${entity}UpdateDTO dto) {
        Assert.notNull(dto.get${thisEntityId}(), SystemConstants.VALUE_IS_NOT_NULL_MSG_TEMPLATE, "${keyPropertyName}");
        ${entity} ${thisEntity} = BeanCopierUtil.copyProperties(dto, ${entity}.class);
        updateById(${thisEntity});
        return getDetail(${thisEntity}.get${thisEntityId}());
    }


    /**
     * ${thisName}删除
     * @param ${keyPropertyName} Long
     */
    public void deleteById(Long ${keyPropertyName}) {
        Assert.notNull(${keyPropertyName}, SystemConstants.VALUE_IS_NOT_NULL_MSG_TEMPLATE, "${keyPropertyName}");
        removeById(${keyPropertyName});
    }


    /**
     * ${thisName}根据${keyPropertyName}批量删除
     * @param ${keyPropertyName}List List<Long>
     */
    public void deleteByIdList(List<Long> ${keyPropertyName}List) {
        if (CollUtil.isEmpty(${keyPropertyName}List)) {
            return;
        }
        baseMapper.deleteBatchIds(${keyPropertyName}List);
    }


    /**
     * 获取列表查询条件
     *
     * @param query ${thisQuery}
     * @return LambdaQueryWrapper
     */
    private LambdaQueryWrapper<${entity}> getListQueryWrapper(${thisQuery} query) {

        ${entity} ${thisEntity} = BeanCopierUtil.copyProperties(query, ${entity}.class);
        return new LambdaQueryWrapper<>(${thisEntity});

    }

}
</#if>
