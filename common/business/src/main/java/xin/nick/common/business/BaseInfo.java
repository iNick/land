package xin.nick.common.business;

import lombok.Data;

/**
 * @author Nick
 * @since 2024/1/26
 */
@Data
public class BaseInfo {
    // 有公共的业务可以尝试放这个包 business
}
