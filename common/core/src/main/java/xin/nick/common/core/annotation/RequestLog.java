package xin.nick.common.core.annotation;


import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 用于标记Controller层方法,
 * 打印入参和出参
 *
 * @author Nick
 * @see xin.nick.common.core.aspect.AopLogAspect
 * @since 2022/1/24
 */
@Target({METHOD, TYPE})
@Retention(RUNTIME)
public @interface RequestLog {
}
