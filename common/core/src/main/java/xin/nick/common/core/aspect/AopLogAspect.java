package xin.nick.common.core.aspect;

import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import xin.nick.common.core.entity.LogRequestInfo;
import xin.nick.common.core.util.IpUtil;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;

/**
 * 请求参数打印
 *
 * @author Nick
 * @since 2022/1/24
 */
@Slf4j
@Aspect
@Configuration
public class AopLogAspect {

    @PostConstruct
    public void initMethod() {
        log.info("日志切面创建....");
    }

    /**
     * 根据注解进行切面
     */
//    @Pointcut("execution(public * xin.nick.**.controller..*.*(..))")
    @Pointcut("@within(xin.nick.common.core.annotation.RequestLog)")
    public void webLogAnnotation() {
    }

    @Around("webLogAnnotation()")
    public Object handleByAnnotation(ProceedingJoinPoint joinPoint) throws Throwable {

        try {
            log.info("请求日志记录开始:");
            LogRequestInfo logRequestInfo = preHandle(joinPoint);
            log.info(logRequestInfo.toString());
        } catch (Exception e) {
            log.warn("打印输入参数出错: {}", e.getMessage());
        }

        long begin = System.currentTimeMillis();

        // 原方法
        Object result = joinPoint.proceed();

        try {
            log.info("REQUEST DURATION(AOP)     : {} ms", (System.currentTimeMillis() - begin));
            log.info("REQUEST RESULT(AOP)       : {}", JSON.toJSONString(result));
            log.info("请求日志记录结束");
        } catch (Exception e) {
            log.warn("打印返回参数出错: {}", e.getMessage());
        }


        return result;
    }

    private LogRequestInfo preHandle(ProceedingJoinPoint joinPoint) {

        LogRequestInfo logRequestInfo = new LogRequestInfo();

        // 准备取出参数信息
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = Objects.requireNonNull(attributes).getRequest();
        Signature signature = joinPoint.getSignature();
        String method = request.getMethod();
        String contentType = request.getContentType();
        String paramString;
        if (Objects.equals(contentType, MediaType.APPLICATION_JSON_VALUE)) {
            paramString = JSON.toJSONString(joinPoint.getArgs()[0]);
        } else {
            Map<String, String[]> parameterMap = request.getParameterMap();
            paramString = JSON.toJSONString(parameterMap);
        }
        String ipAddress = IpUtil.getIpAddress(request);

        // 组装好返回的内容
        logRequestInfo.setLogType("AOP");
        logRequestInfo.setIp(ipAddress);
        logRequestInfo.setUrl(request.getRequestURL().toString());
        logRequestInfo.setUri(request.getRequestURI());
        logRequestInfo.setMethod(method);
        logRequestInfo.setTypeName(signature.getDeclaringType().toString());
        logRequestInfo.setMethodName(signature.getName());
        logRequestInfo.setUserAgent(request.getHeader("User-Agent"));
        logRequestInfo.setContentType(contentType);
        logRequestInfo.setParam(paramString);

        return logRequestInfo;

    }

}
