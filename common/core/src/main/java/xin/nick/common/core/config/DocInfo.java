package xin.nick.common.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import xin.nick.common.core.constant.SystemConstants;

/**
 * @author Nick
 * @since 2022/11/24
 */
@Data
@Component
@ConfigurationProperties(prefix = "doc-info")
public class DocInfo {
    private String title = SystemConstants.DEFAULT_APPLICATION_NAME;
    private String description = SystemConstants.DEFAULT_APPLICATION_NAME;
    private String version = SystemConstants.DEFAULT_VERSION;
    private String websiteName = SystemConstants.DEFAULT_APPLICATION_NAME;
    private String websiteUrl = SystemConstants.DEFAULT_WEB_SITE_URL;
}
