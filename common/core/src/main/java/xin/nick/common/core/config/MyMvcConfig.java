package xin.nick.common.core.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import xin.nick.common.core.interceptor.MdcInterceptor;

/**
 * MVC配置类
 *
 * @author Nick
 * @since 2022/1/27
 */
@Configuration
@ServletComponentScan
@RequiredArgsConstructor
public class MyMvcConfig implements WebMvcConfigurer {

    private final MdcInterceptor mdcInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(mdcInterceptor).addPathPatterns("/**");
    }

}
