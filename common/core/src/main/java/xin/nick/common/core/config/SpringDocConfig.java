package xin.nick.common.core.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import xin.nick.common.core.constant.SystemConstants;

/**
 * @author Nick
 * @since 2023/1/3/003
 */
@RequiredArgsConstructor
@Configuration
@EnableKnife4j
public class SpringDocConfig {

    private final DocInfo docInfo;

    @Bean
    public OpenAPI defaultOpenAPI() {

        return new OpenAPI()
                .info(new Info().title(docInfo.getTitle())
                        .description(docInfo.getDescription())
                        .version(docInfo.getVersion()))
                .externalDocs(new ExternalDocumentation().description(docInfo.getWebsiteName())
                        .url(docInfo.getWebsiteUrl()))
                .components(new Components().addSecuritySchemes(SystemConstants.HEADER_AUTHORIZATION, new SecurityScheme()
                        .name(SystemConstants.HEADER_AUTHORIZATION)
                        .type(SecurityScheme.Type.HTTP)
                        .scheme("Bearer")))
                ;
    }


//    @Bean
//    public GroupedOpenApi articleApi() {
//        return GroupedOpenApi.builder()
//                .group(docInfo.getTitle() + "_文章模块")
//                .packagesToScan("xin.nick.article.controller")
//                .pathsToMatch("/**")
//                .build();
//    }

    @Bean
    public GroupedOpenApi systemApi() {
        return GroupedOpenApi.builder()
                .group(docInfo.getTitle() + "_接口信息")
//                .packagesToScan("xin.nick.system.controller")
                .pathsToMatch("/**")
                .build();
    }


}
