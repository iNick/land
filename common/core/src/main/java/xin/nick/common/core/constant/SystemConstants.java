package xin.nick.common.core.constant;

/**
 * @author Nick
 */
public class SystemConstants {

    private SystemConstants() {

    }

    public static final String ROOT = "ROOT";

    /**
     * 因为security自身的设计原因，角色权限前面需要添加ROLE前缀
     */
    public static final String ROLE_PREFIX = "ROLE_";

    /**
     * root账户角色
     */
    public static final String ROOT_ROLE = ROLE_PREFIX + ROOT;

    /**
     * 请求id
     */
    public static final String REQUEST_ID = "requestId";

    /**
     * userId
     */
    public static final String USER_ID = "userId";

    /**
     * 请求id
     */
    public static final int REQUEST_STATUS_OK = 0;

    /**
     * 参数错误
     */
    public static final int REQUEST_PARAM_ERROR = 400;


    /**
     * 参数异常
     */
    public static final String REQUEST_PARAM_ERROR_MSG = "参数异常";

    /**
     * 服务器错误
     */
    public static final int REQUEST_SERVER_ERROR = 500;


    /**
     * 服务器错误
     */
    public static final String REQUEST_SERVER_ERROR_MSG = "服务器异常,请联系管理员";

    /**
     * 自定义错误
     */
    public static final int REQUEST_CUSTOM_ERROR = 1500;


    /**
     * 错误处理器
     */
    public static final String ERROR_PATH = "/error";

    /**
     * 登录地址
     */
    public static final String LOGIN_PATH = "/login";


    /**
     * API地址
     */
    public static final String API_PATH = "/api";

    /**
     * API地址
     */
    public static final String API_PATH_REGULAR = "/api/**";

    /**
     * 退出登录地址
     */
    public static final String LOGOUT_PATH = "/logout";

    /**
     * 退出登录地址
     */
    public static final String CODE_PATH = "/code";

    /**
     * 退出登录地址
     */
    public static final String REFRESH_TOKEN_PATH = "/api/refresh/token";

    /**
     * 项目名字
     */
    public static final String APP_NAME = "land";

    /**
     * 用户token缓存key
     */
    public static final String PROJECT_PORT = "9420";

    /**
     * redis前缀
     */
    public static final String REDIS_APP_KEY = APP_NAME + ":" + PROJECT_PORT + ":";

    /**
     * 用户token缓存key
     */
    public static final String USER_TOKEN_KEY = REDIS_APP_KEY + "user_token:";


    /**
     * 用户token缓存 过期时间,三十分钟
     */
    public static final Long USER_TOKEN_EXPIRE = 60L * 30L;

    /**
     * UTF8
     */
    public static final String UTF8 = "UTF-8";

    /**
     * header Authorization
     */
    public static final String HEADER_AUTHORIZATION = "Authorization";

    /**
     * token获取的key
     */
    public static final String TOKEN_KEY = "token";


    // Spring Security 登录用户名
    public static final String SPRING_SECURITY_FORM_USERNAME_KEY = "username";

    // Spring Security 登录密码
    public static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "password";

    // limit 1
    public static final String LIMIT_1 = " limit 1 ";

    public static final String DEFAULT_APPLICATION_NAME = "Land管理系统";

    public static final String DEFAULT_WEB_SITE_URL = "https://nick.xin/";
    public static final String DEFAULT_VERSION = "v1.20240130";
    public static final String VALUE_IS_NOT_NULL_MSG_TEMPLATE = "[{}]不可为空";




}
