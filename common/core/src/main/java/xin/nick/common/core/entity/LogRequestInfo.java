package xin.nick.common.core.entity;

import lombok.Data;

/**
 * 请求信息
 *
 * @author Nick
 * @since 2022/1/24
 */
@Data
public class LogRequestInfo {

    /**
     * 日志记录类型
     */
    private String logType;

    /**
     * ip
     */
    private String ip;

    /**
     * 请求uri
     */
    private String uri;

    /**
     * 请求url
     */
    private String url;

    /**
     * 请求方式
     */
    private String method;

    /**
     * 类名
     */
    private String typeName;

    /**
     * 方法名
     */
    private String methodName;

    /**
     * 用户代理
     */
    private String userAgent;

    /**
     * 内容类型
     */
    private String contentType;

    /**
     * 请求参数
     */
    private String param;

}
