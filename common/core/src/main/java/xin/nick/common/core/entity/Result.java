package xin.nick.common.core.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.slf4j.MDC;
import xin.nick.common.core.constant.SystemConstants;

/**
 * @author Nick
 * @date 2021/11/5
 */
@Data
public class Result<T> {

    @Schema(description = "状态代码")
    private int code;

    @Schema(description = "返回消息")
    private String msg;

    @Schema(description = "返回数据")
    private T data;

    @Schema(description = "请求id")
    private String requestId;

    /**
     * 初始化一个新创建的 Result 对象，使其表示一个空消息。
     */
    public Result() {
        this.code = SystemConstants.REQUEST_STATUS_OK;
        this.requestId = MDC.get(SystemConstants.REQUEST_ID);
    }

    public Result(ResultCode resultCode) {
        this.code = resultCode.getValue();
        this.msg = resultCode.getDescription();
        this.requestId = MDC.get(SystemConstants.REQUEST_ID);
    }

    public Result(String msg) {
        this.code = SystemConstants.REQUEST_STATUS_OK;
        this.msg = msg;
        this.requestId = MDC.get(SystemConstants.REQUEST_ID);
    }

    public Result(T data) {
        this.code = SystemConstants.REQUEST_STATUS_OK;
        this.data = data;
        this.requestId = MDC.get(SystemConstants.REQUEST_ID);
    }

    public Result(String msg, T data) {
        this.code = SystemConstants.REQUEST_STATUS_OK;
        this.msg = msg;
        this.data = data;
        this.requestId = MDC.get(SystemConstants.REQUEST_ID);
    }

    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
        this.requestId = MDC.get(SystemConstants.REQUEST_ID);
    }

    public Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.requestId = MDC.get(SystemConstants.REQUEST_ID);
    }


    /**
     * 返回成功消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static <T> Result<T> success(String msg, T data) {
        return new Result<>(msg, data);
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static Result<Void> success(String msg) {
        return new Result<>(msg);
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static <T> Result<T> success(T data) {
        return new Result<>(data);
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static <T> Result<T> success() {
        return new Result<>();
    }


    /**
     * 返回错误消息
     *
     * @return 警告消息
     */
    public static <T> Result<T> error() {
        return new Result<>(SystemConstants.REQUEST_SERVER_ERROR, SystemConstants.REQUEST_SERVER_ERROR_MSG);
    }


    /**
     * 返回错误消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static <T> Result<T> error(String msg) {
        return new Result<>(SystemConstants.REQUEST_SERVER_ERROR, msg);
    }

    /**
     * 返回错误消息和数据
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static <T> Result<T> error(String msg, T data) {
        return new Result<>(SystemConstants.REQUEST_SERVER_ERROR, msg, data);
    }

    /**
     * 参数异常
     *
     * @return 警告消息
     */
    public static <T> Result<T> paramError() {
        return new Result<>(SystemConstants.REQUEST_PARAM_ERROR, SystemConstants.REQUEST_PARAM_ERROR_MSG);
    }

    /**
     * 参数异常
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static <T> Result<T> paramError(String msg) {
        return new Result<>(SystemConstants.REQUEST_PARAM_ERROR, msg);
    }


    /**
     * 返回自定义消息
     *
     * @return 警告消息
     */
    public static <T> Result<T> custom(ResultCode resultCode) {
        return new Result<>(resultCode);
    }

    /**
     * 返回自定义消息
     *
     * @return 警告消息
     */
    public static <T> Result<T> custom(ResultCode resultCode, String msg) {
        return new Result<>(resultCode.getValue(), msg);
    }


    /**
     * 返回自定义消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static <T> Result<T> custom(String msg) {
        return new Result<>(SystemConstants.REQUEST_CUSTOM_ERROR, msg);
    }

    /**
     * 返回自定义消息
     *
     * @param code 状态码
     * @param msg  返回内容
     * @return 警告消息
     */
    public static <T> Result<T> custom(int code, String msg) {
        return new Result<>(code, msg);
    }


    /**
     * 返回错误消息
     *
     * @param code 状态码
     * @param msg  返回内容
     * @return 警告消息
     */
    public static <T> Result<T> custom(int code, String msg, T data) {
        return new Result<>(code, msg, data);
    }


}



