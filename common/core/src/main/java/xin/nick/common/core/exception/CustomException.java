package xin.nick.common.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.http.HttpStatus;
import xin.nick.common.core.entity.ResultCode;

/**
 * 自定义服务器异常
 *
 * @author Nick
 * @since 2022/7/27/027
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class CustomException extends RuntimeException {


    private final Integer code;

    private final String message;

    public CustomException() {
        this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
        this.message = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
    }

    public CustomException(String message) {
        this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
        this.message = message;
    }

    public CustomException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public CustomException(ResultCode resultCode) {
        this.code = resultCode.getValue();
        this.message = resultCode.getDescription();
    }

}
