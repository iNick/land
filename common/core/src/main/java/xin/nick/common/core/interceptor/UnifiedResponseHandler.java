package xin.nick.common.core.interceptor;

import com.alibaba.fastjson2.JSON;
import org.springframework.core.MethodParameter;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import xin.nick.common.core.annotation.UnifiedResult;
import xin.nick.common.core.entity.Result;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * 处理统一返回响应信息
 *
 * @author Nick
 */
@ControllerAdvice
public class UnifiedResponseHandler implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(MethodParameter methodParameter, @NonNull Class converterTypeClass) {


        // 获取 方法 和 class 对象
        Method method = methodParameter.getMethod();
        Class<?> clazz = Objects.requireNonNull(method, "method is null").getDeclaringClass();

        //如果是FileSystemResource 则不拦截
        if (method.getAnnotatedReturnType().getType().getTypeName()
                .equals(FileSystemResource.class.getTypeName())) {
            return false;
        }

        // 只处理 ResponseResult 标注的类或方法
        Annotation annotation = clazz.getAnnotation(UnifiedResult.class);
        if (Objects.isNull(annotation)) {
            annotation = method.getAnnotation(UnifiedResult.class);
        }

        // 存在则返回
        return annotation != null;
    }

    @Override
    public Object beforeBodyWrite(Object o,
                                  @NonNull MethodParameter methodParameter,
                                  @NonNull MediaType mediaType,
                                  @NonNull Class selectedConverterTypeClass,
                                  @NonNull ServerHttpRequest serverHttpRequest,
                                  @NonNull ServerHttpResponse serverHttpResponse) {


        // 返回数据为 String 且不是 xml 类型的,进行处理
        if ((o instanceof String) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            // 转换类型为 JSON
            serverHttpResponse.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            return JSON.toJSONString(Result.success(o));
        }

        // 返回数据为 null 且为 html 类型的,进行处理
        if (Objects.isNull(o) && MediaType.TEXT_HTML.equals(mediaType)) {
            // 转换类型为 JSON
            serverHttpResponse.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            return Result.success();
        }


        // 处理格式为 JSON 类型的
        if (MediaType.APPLICATION_JSON.equals(mediaType)) {
            // 如果是 Result对象直接返回
            // 其他的对象都封装为Result
            if (o instanceof Result) {
                return o;
            } else {
                return Result.success(o);
            }
        } else {
            return o;
        }

    }
}
