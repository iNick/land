package xin.nick.common.core.query;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class PageQuery {

    /**
     * 页面大小
     */
    @Schema(description = "页面大小,默认为10", example = "10")
    private Long size = 10L;

    /**
     * 当前页面
     */
    @Schema(description = "当前页面,默认为1", example = "1")
    private Long current = 1L;
}
