package xin.nick.common.core.util;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;

import java.security.Key;

/**
 * @author Nick
 * @since 2023/3/24/024
 */
public class JwtUtil {

    public static String createJwt(JwtClaims jwtClaims, String secret) throws JoseException {
        Key hmacKey = new HmacKey(secret.getBytes());
        JsonWebSignature jsonWebSignature = new JsonWebSignature();
        jsonWebSignature.setHeader("typ", "JWT");
        // 为 JsonWebSignature 对象添加负载：JwtClaims 对象的 Json 内容
        jsonWebSignature.setPayload(jwtClaims.toJson());
        // JWT 使用  私钥签名
        jsonWebSignature.setKey(hmacKey);
        // 在 JWT / JWS 上设置签名算法
        jsonWebSignature.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);

        return jsonWebSignature.getCompactSerialization();
    }

    /**
     * 获取 JwtClaims
     *
     * @param jwtString String
     * @param secret    String
     * @return JwtClaims
     * @throws InvalidJwtException InvalidJwtException
     */
    public static JwtClaims getJwtClaims(String jwtString, String secret) throws InvalidJwtException {

        Key hmacKey = new HmacKey(secret.getBytes());
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                // 验证时间兼容
                .setAllowedClockSkewInSeconds(30)
                .setRequireExpirationTime()
                .setRequireSubject()
                // 设置用于验证签名的公钥
                .setVerificationKey(hmacKey)
                .build();


        return jwtConsumer.processToClaims(jwtString);
    }

    /**
     * 创建 JwtClaims
     *
     * @param sub
     * @return
     */
    public static JwtClaims createJwtClaims(String sub) {
        JwtClaims claims = new JwtClaims();
        // 设置 Token 发布/创建 时间为当前时间
        claims.setIssuedAtToNow();
        // 生效时间
        claims.setNotBeforeMinutesInThePast(0);
        // 设置过期时间
        claims.setExpirationTimeMinutesInTheFuture(24 * 60);
        // 为 JWT 设置一个自动生成的唯一 ID
        claims.setGeneratedJwtId();
        // 负载对象
        claims.setSubject(sub);
        return claims;
    }

    public static void main(String[] args) throws JoseException, InvalidJwtException {
        String secretString = "CauseImaFoxAndILikeYou1314123456789";
        String issuer = "nick";
        JwtClaims jwtClaims = JwtUtil.createJwtClaims(String.valueOf(1));
        // jwtClaims属性
        jwtClaims.setIssuer(issuer);
        String jwt = JwtUtil.createJwt(jwtClaims, secretString);
        System.out.println(jwt);

        JwtClaims meClaims = JwtUtil.getJwtClaims(jwt, secretString);
        System.out.println(meClaims);
    }

}
