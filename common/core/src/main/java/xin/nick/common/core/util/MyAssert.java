package xin.nick.common.core.util;

import cn.hutool.core.lang.Assert;
import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;
import xin.nick.common.core.entity.ResultCode;
import xin.nick.common.core.exception.CustomException;

import java.util.Map;
import java.util.function.Supplier;

/**
 * @author Nick
 * @date 2022/8/1
 */
public class MyAssert extends Assert {

    public static void notNull(@Nullable Object object, String message) {
        if (object == null) {
            throw new CustomException(message);
        }
    }

    public static void notNull(@Nullable Object object, ResultCode resultCode) {
        if (object == null) {
            throw new CustomException(resultCode);
        }
    }

    public static void isNull(@Nullable Object object, String message) {
        if (object != null) {
            throw new CustomException(message);
        }
    }

    public static void isNull(@Nullable Object object, ResultCode resultCode) {
        if (object != null) {
            throw new CustomException(resultCode);
        }
    }

    public static void isTrue(boolean expression, String message) {
        if (!expression) {
            throw new CustomException(message);
        }
    }

    public static void isTrue(boolean expression, ResultCode resultCode) {
        if (!expression) {
            throw new CustomException(resultCode);
        }
    }


    public static void notEmpty(@Nullable Map<?, ?> map, String message) {
        if (CollectionUtils.isEmpty(map)) {
            throw new CustomException(message);
        }
    }

    public static void notEmpty(@Nullable Map<?, ?> map, ResultCode resultCode) {
        if (CollectionUtils.isEmpty(map)) {
            throw new CustomException(resultCode);
        }
    }

    public static void isInstanceOf(Class<?> type, @Nullable Object obj, String message) {
        notNull(type, (String) "Type to check against must not be null");
        if (!type.isInstance(obj)) {
            throw new CustomException(message);
        }

    }

    public static void isInstanceOf(Class<?> type, @Nullable Object obj, ResultCode resultCode) {
        notNull(type, (String) "Type to check against must not be null");
        if (!type.isInstance(obj)) {
            throw new CustomException(resultCode);
        }

    }

    public static void isInstanceOf(Class<?> type, @Nullable Object obj, Supplier<String> messageSupplier) {
        notNull(type, (String) "Type to check against must not be null");
        if (!type.isInstance(obj)) {
            throw new CustomException(nullSafeGet(messageSupplier));
        }
    }

    public static void ifInstanceOfThrow(Class<?> type, @Nullable Object obj, ResultCode resultCode) {
        notNull(type, (String) "Type to check against must not be null");
        if (type.isInstance(obj)) {
            throw new CustomException(resultCode);
        }
    }

    public static void throwException(ResultCode resultCode) {
        throw new CustomException(resultCode);
    }

    public static void throwException(Integer code, String message) {
        throw new CustomException(code, message);
    }

    public static void throwException(String message) {
        throw new CustomException(message);
    }


    @Nullable
    private static String nullSafeGet(@Nullable Supplier<String> messageSupplier) {
        return messageSupplier != null ? (String) messageSupplier.get() : null;
    }


}
