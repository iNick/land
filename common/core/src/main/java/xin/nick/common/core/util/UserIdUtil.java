package xin.nick.common.core.util;

import cn.hutool.core.lang.Assert;
import xin.nick.common.core.entity.ResultCode;

/**
 * 用户id
 *
 * @author Nick
 * @date 2021/11/9
 */
public class UserIdUtil {

    private static final ThreadLocal<Long> USER_ID_THREAD_LOCAL = new ThreadLocal<>();

    public static void setUserId(Long userId) {
        USER_ID_THREAD_LOCAL.set(userId);
    }

    /**
     * 获取存入的用户ID
     * @return
     */
    public static Long getUserId() {
        Long userId = USER_ID_THREAD_LOCAL.get();
        Assert.notNull(userId, ResultCode.NOT_FOUND_USER_ID_ERROR.getDescription());
        return userId;
    }

    public static Long getUserIdNotCheck() {
        return USER_ID_THREAD_LOCAL.get();
    }

    public static void removeUserId() {
        USER_ID_THREAD_LOCAL.remove();
    }
}
