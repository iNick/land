package xin.nick;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;

/**
 * @author Nick
 * @since 2022/12/12
 */
@SpringBootApplication
@Slf4j
public class LandCmsApplication {


    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(LandCmsApplication.class, args);

        ConfigurableEnvironment environment = applicationContext.getEnvironment();

        log.info("LandCmsApplication started!!!");
        log.info("Land管理系统-CMS启动!!!");

        try {
            String ip = InetAddress.getLocalHost().getHostAddress();
            String port = Optional.ofNullable(environment.getProperty("server.port")).orElse("8080");
            String contextPath = Optional.ofNullable(environment.getProperty("server.servlet.context-path")).orElse("");
            log.info("http://{}:{}{}/doc.html", ip, port, contextPath);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

}
