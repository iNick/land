package xin.nick.system.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nick
 * @since 2023/9/20
 */
@Data
@Component
@ConfigurationProperties(prefix = "application.security")
public class ApplicationSecurityProperties {

    /**
     * 放行不校验的配置，不校验此处列表请求
     */
    private List<String> ignoreList = new ArrayList<>();

    /**
     * 放行白名单配置，不校验此处的白名单
     */
    private List<String> whiteList = new ArrayList<>();

    /**
     * 需要校验的路径
     */
    private List<String> authList = new ArrayList<>();

}
