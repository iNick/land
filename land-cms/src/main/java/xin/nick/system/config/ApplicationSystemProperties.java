package xin.nick.system.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Nick
 * @since 2023/1/3
 */
@Data
@Component
@ConfigurationProperties(prefix = "application.system")
public class ApplicationSystemProperties {

    private Long rootId = 1L;

    private Integer defaultAgentId = 1000001;

    private String localFileSavePath = "/var/file/";

    private String fileRequestPath = "/api/files";

    private String projectUrl = "http://127.0.0.1:9420";

    private String defaultPassword = "123456";

    private String jwtSecret = "CauseImaFoxAndILikeYou1314123456789";

    private String aesSecret = "CauseFoxILikeYou";


}
