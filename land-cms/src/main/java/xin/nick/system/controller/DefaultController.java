package xin.nick.system.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.common.core.constant.SystemConstants;
import xin.nick.system.domain.dto.LoginDTO;
import xin.nick.system.domain.vo.LoginUserVO;

import javax.validation.Valid;

/**
 * 注意,这个类并没有实际的处理
 * @author Nick
 * @since 2023/11/23
 */
@Tag(name = "默认接口")
@RestController
public class DefaultController {

    @Operation(summary = "登录")
    @PostMapping("/login")
    public LoginUserVO login(@RequestBody @Valid LoginDTO loginDTO) {
        // 注意,登录逻辑通过 Security 过滤器进行处理, @See WebSecurityConfig
        return new LoginUserVO();
    }

    @Operation(summary = "退出登录",
            security = { @SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION) })
    @DeleteMapping("/logout")
    public void logout() {
        // 注意,退出登录逻辑通过 Security 过滤器进行处理, @See WebSecurityConfig
    }

}
