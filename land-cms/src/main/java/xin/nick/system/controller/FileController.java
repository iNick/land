package xin.nick.system.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import xin.nick.common.core.annotation.RequestLog;
import xin.nick.common.core.annotation.UnifiedResult;
import xin.nick.common.core.constant.SystemConstants;
import xin.nick.system.domain.vo.FileUploadVO;
import xin.nick.system.service.LocalFileService;

import java.util.Objects;

/**
 * 文件管理
 *
 * @author Nick
 * @since 2022/2/10
 */
@Tag(name = "文件上传")
@RestController
@RequestMapping("/file")
@RequiredArgsConstructor
@UnifiedResult
@RequestLog
public class FileController {

    private final LocalFileService localFileService;

    @Operation(summary = "普通文件上传",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PostMapping(value = "/uploadFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('file:upload')")
    public FileUploadVO fileUpload(@RequestParam("file") MultipartFile file) {

        //判断文件是否为空
        boolean fileIsEmpty = Objects.isNull(file) || file.isEmpty();
        Assert.isTrue(!fileIsEmpty, "上传的文件为空");

        return localFileService.uploadFile(file);

    }

}
