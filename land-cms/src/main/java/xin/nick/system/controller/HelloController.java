package xin.nick.system.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.common.core.annotation.RequestLog;
import xin.nick.common.core.annotation.UnifiedResult;

/**
 * @author Nick
 * @since 2023/5/29
 */
@Tag(name = "测试Hello")
@RestController
@UnifiedResult
@RequestLog
public class HelloController {

    @GetMapping("/hello")
    public String sayHello(String message) {
        return "Hello " + message;
    }
}
