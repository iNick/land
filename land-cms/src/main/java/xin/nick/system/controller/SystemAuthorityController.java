package xin.nick.system.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.common.core.annotation.RequestLog;
import xin.nick.common.core.annotation.UnifiedResult;
import xin.nick.common.core.constant.SystemConstants;
import xin.nick.system.domain.dto.SystemAuthorityCreateDTO;
import xin.nick.system.domain.dto.SystemAuthorityUpdateDTO;
import xin.nick.system.domain.query.SystemAuthorityQuery;
import xin.nick.system.domain.vo.SystemAuthorityVO;
import xin.nick.system.service.SystemAuthorityService;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统-权限 前端控制器
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Tag(name = "系统-权限模块")
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/system-authority")
@UnifiedResult
@RequestLog
public class SystemAuthorityController {

    private final SystemAuthorityService systemAuthorityService;

    @Operation(summary = "系统-权限详情",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getDetail/{authorityId}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-authority:detail')")
    public SystemAuthorityVO getDetail(@PathVariable(name = "authorityId") Long authorityId) {
        return systemAuthorityService.getDetail(authorityId);
    }

    @Operation(summary = "系统-权限列表",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getList")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-authority:list')")
    public List<SystemAuthorityVO> getList(@ParameterObject SystemAuthorityQuery query) {
        return systemAuthorityService.getList(query);
    }

    @Operation(summary = "系统-权限列表分页",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getListPage")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-authority:list')")
    public Page<SystemAuthorityVO> getListPage(@ParameterObject SystemAuthorityQuery query) {
        return systemAuthorityService.getListPage(query);
    }

    @Operation(summary = "系统-权限创建",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PostMapping("/create")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-authority:create')")
    public SystemAuthorityVO create(@RequestBody @Valid SystemAuthorityCreateDTO dto) {
        return systemAuthorityService.createSystemAuthority(dto);
    }

    @Operation(summary = "系统-权限更新",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PutMapping("/update")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-authority:update')")
    public SystemAuthorityVO update(@RequestBody @Valid SystemAuthorityUpdateDTO dto) {
        return systemAuthorityService.updateSystemAuthority(dto);
    }

    @Operation(summary = "系统-权限删除",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @DeleteMapping("/delete/{authorityId}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-authority:delete')")
    public void delete(@PathVariable(name = "authorityId") Long authorityId) {
        systemAuthorityService.deleteById(authorityId);
    }

    @Operation(summary = "系统-权限批量删除",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @DeleteMapping("/deleteByIdList/{authorityIdList}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-authority:delete')")
    public void delete(@PathVariable(name = "authorityIdList") List<Long> authorityIdList) {
        systemAuthorityService.deleteByIdList(authorityIdList);
    }

    @Operation(summary = "启用|禁用",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PutMapping("/updateDisabled/{authorityId}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-authority:delete')")
    public void updateDisabled(@PathVariable(name = "authorityId") Long authorityId) {
        systemAuthorityService.updateDisabled(authorityId);
    }

}
