package xin.nick.system.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import xin.nick.common.core.constant.SystemConstants;
import xin.nick.system.service.SystemConfigService;
import xin.nick.system.domain.dto.SystemConfigUpdateDTO;
import xin.nick.system.domain.dto.SystemConfigCreateDTO;
import xin.nick.system.domain.query.SystemConfigQuery;
import xin.nick.system.domain.vo.SystemConfigVO;
import xin.nick.common.core.annotation.RequestLog;
import xin.nick.common.core.annotation.UnifiedResult;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 系统配置 前端控制器
 * </p>
 *
 * @author Nick
 * @since 2023-11-28
 */
@Tag(name = "系统配置模块")
@UnifiedResult
@RequestLog
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/system-config")
public class SystemConfigController {

    private final SystemConfigService systemConfigService;

    @Operation(summary = "系统配置详情",
            security = { @SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION) })
    @GetMapping("/getDetail/{configId}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-config:detail')")
    public SystemConfigVO getDetail(@PathVariable(name = "configId") Long configId) {
        return systemConfigService.getDetail(configId);
    }

    @Operation(summary = "系统配置列表",
            security = { @SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION) })
    @GetMapping("/getList")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-config:list')")
    public List<SystemConfigVO> getList(@ParameterObject SystemConfigQuery query) {
        return systemConfigService.getList(query);
    }

    @Operation(summary = "系统配置列表分页",
            security = { @SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION) })
    @GetMapping("/getListPage")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-config:list')")
    public Page<SystemConfigVO> getListPage(@ParameterObject SystemConfigQuery query) {
        return systemConfigService.getListPage(query);
    }

    @Operation(summary = "系统配置创建",
            security = { @SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION) })
    @PostMapping("/create")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-config:create')")
    public SystemConfigVO create(@RequestBody @Valid SystemConfigCreateDTO dto) {
        return systemConfigService.createSystemConfig(dto);
    }

    @Operation(summary = "系统配置更新",
            security = { @SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION) })
    @PutMapping("/update")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-config:update')")
    public SystemConfigVO update(@RequestBody @Valid SystemConfigUpdateDTO dto) {
        return systemConfigService.updateSystemConfig(dto);
    }

    @Operation(summary = "系统配置删除",
            security = { @SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION) })
    @DeleteMapping("/delete/{configId}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-config:delete')")
    public void delete(@PathVariable(name = "configId") Long configId) {
        systemConfigService.deleteById(configId);
    }

    @Operation(summary = "系统配置批量删除",
            security = { @SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION) })
    @DeleteMapping("/deleteByIdList/{configIdList}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-config:delete')")
    public void delete(@PathVariable(name = "configIdList") List<Long> configIdList) {
        systemConfigService.deleteByIdList(configIdList);
    }

}
