package xin.nick.system.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.common.core.annotation.RequestLog;
import xin.nick.common.core.annotation.UnifiedResult;
import xin.nick.common.core.constant.SystemConstants;
import xin.nick.system.domain.dto.SystemRoleAuthorityCreateDTO;
import xin.nick.system.domain.dto.SystemRoleAuthorityUpdateDTO;
import xin.nick.system.domain.query.SystemRoleAuthorityQuery;
import xin.nick.system.domain.vo.SystemRoleAuthorityVO;
import xin.nick.system.service.SystemRoleAuthorityService;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统-角色权限关联 前端控制器
 * </p>
 *
 * @author Nick
 * @since 2023-11-08
 */
@Tag(name = "系统-角色权限关联模块")
@UnifiedResult
@RequestLog
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/system-role-authority")
public class SystemRoleAuthorityController {

    private final SystemRoleAuthorityService systemRoleAuthorityService;

    @Operation(summary = "系统-角色权限关联详情",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getDetail/{id}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role-authority:detail')")
    public SystemRoleAuthorityVO getDetail(@PathVariable(name = "id") Long id) {
        return systemRoleAuthorityService.getDetail(id);
    }

    @Operation(summary = "系统-角色权限关联列表",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getList")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role-authority:list')")
    public List<SystemRoleAuthorityVO> getList(@ParameterObject SystemRoleAuthorityQuery query) {
        return systemRoleAuthorityService.getList(query);
    }

    @Operation(summary = "系统-角色权限关联列表分页",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getListPage")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role-authority:list')")
    public Page<SystemRoleAuthorityVO> getListPage(@ParameterObject SystemRoleAuthorityQuery query) {
        return systemRoleAuthorityService.getListPage(query);
    }

    @Operation(summary = "系统-角色权限关联创建",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PostMapping("/create")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role-authority:create')")
    public SystemRoleAuthorityVO create(@RequestBody @Valid SystemRoleAuthorityCreateDTO dto) {
        return systemRoleAuthorityService.createSystemRoleAuthority(dto);
    }

    @Operation(summary = "系统-角色权限关联更新",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PutMapping("/update")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role-authority:update')")
    public SystemRoleAuthorityVO update(@RequestBody @Valid SystemRoleAuthorityUpdateDTO dto) {
        return systemRoleAuthorityService.updateSystemRoleAuthority(dto);
    }

    @Operation(summary = "系统-角色权限关联删除",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role-authority:delete')")
    public void delete(@PathVariable(name = "id") Long id) {
        systemRoleAuthorityService.deleteById(id);
    }

    @Operation(summary = "系统-角色权限关联批量删除",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @DeleteMapping("/deleteByIdList/{idList}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role-authority:delete')")
    public void delete(@PathVariable(name = "idList") List<Long> idList) {
        systemRoleAuthorityService.deleteByIdList(idList);
    }

}
