package xin.nick.system.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.common.core.annotation.RequestLog;
import xin.nick.common.core.annotation.UnifiedResult;
import xin.nick.common.core.constant.SystemConstants;
import xin.nick.system.domain.dto.SystemRoleCreateDTO;
import xin.nick.system.domain.dto.SystemRoleUpdateDTO;
import xin.nick.system.domain.query.SystemRoleQuery;
import xin.nick.system.domain.vo.SystemRoleVO;
import xin.nick.system.service.SystemRoleService;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统-角色 前端控制器
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Tag(name = "系统-角色模块")
@UnifiedResult
@RequestLog
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/system-role")
public class SystemRoleController {

    private final SystemRoleService systemRoleService;

    @Operation(summary = "系统-角色详情",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getDetail/{roleId}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role:detail')")
    public SystemRoleVO getDetail(@PathVariable(name = "roleId") Long roleId) {
        return systemRoleService.getDetail(roleId);
    }

    @Operation(summary = "系统-角色列表",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getList")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role:list')")
    public List<SystemRoleVO> getList(@ParameterObject SystemRoleQuery query) {
        return systemRoleService.getList(query);
    }

    @Operation(summary = "系统-角色列表分页",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getListPage")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role:list')")
    public Page<SystemRoleVO> getListPage(@ParameterObject SystemRoleQuery query) {
        return systemRoleService.getListPage(query);
    }

    @Operation(summary = "系统-角色创建",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PostMapping("/create")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role:create')")
    public SystemRoleVO create(@RequestBody @Valid SystemRoleCreateDTO dto) {
        return systemRoleService.createSystemRole(dto);
    }

    @Operation(summary = "系统-角色更新",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PutMapping("/update")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role:update')")
    public SystemRoleVO update(@RequestBody @Valid SystemRoleUpdateDTO dto) {
        return systemRoleService.updateSystemRole(dto);
    }

    @Operation(summary = "系统-角色删除",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @DeleteMapping("/delete/{roleId}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role:delete')")
    public void delete(@PathVariable(name = "roleId") Long roleId) {
        systemRoleService.deleteById(roleId);
    }

    @Operation(summary = "系统-角色批量删除",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @DeleteMapping("/deleteByIdList/{roleIdList}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-role:delete')")
    public void delete(@PathVariable(name = "roleIdList") List<Long> roleIdList) {
        systemRoleService.deleteByIdList(roleIdList);
    }

    @RequestLog
    @Operation(summary = "启用|禁用角色",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PutMapping("/updateDisabled/{roleId}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('role:update')")
    public void updateDisabled(@PathVariable(name = "roleId") Long roleId) {
        systemRoleService.updateDisabled(roleId);
    }


}
