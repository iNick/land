package xin.nick.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.common.core.annotation.RequestLog;
import xin.nick.common.core.annotation.UnifiedResult;
import xin.nick.common.core.constant.SystemConstants;
import xin.nick.system.domain.dto.ChangePasswordDTO;
import xin.nick.system.domain.dto.SystemUserCreateDTO;
import xin.nick.system.domain.dto.SystemUserUpdateDTO;
import xin.nick.system.domain.query.SystemUserQuery;
import xin.nick.system.domain.vo.CurrentUserInfoVO;
import xin.nick.system.domain.vo.SystemUserVO;
import xin.nick.system.service.SystemUserService;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统用户信息 前端控制器
 * </p>
 *
 * @author Nick
 * @since 2023-01-03
 */
@Tag(name = "系统-用户模块")
@UnifiedResult
@RequestLog
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/system-user")
public class SystemUserController {

    private final SystemUserService systemUserService;


    @Operation(summary = "获取用户信息",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getUserInfo")
    public CurrentUserInfoVO getUserInfo() {
        return systemUserService.getCurrentUserInfo();
    }

    @Operation(summary = "系统-用户详情",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getDetail/{userId}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user:detail')")
    public SystemUserVO getDetail(@PathVariable(name = "userId") Long userId) {
        return systemUserService.getDetail(userId);
    }

    @Operation(summary = "系统-用户列表",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getList")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user:list')")
    public List<SystemUserVO> getList(@ParameterObject SystemUserQuery query) {
        return systemUserService.getList(query);
    }

    @Operation(summary = "系统-用户列表分页",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getListPage")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user:list')")
    public Page<SystemUserVO> getListPage(@ParameterObject SystemUserQuery query) {
        return systemUserService.getListPage(query);
    }

    @Operation(summary = "系统-用户创建",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PostMapping("/create")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user:create')")
    public SystemUserVO create(@RequestBody @Valid SystemUserCreateDTO dto) {
        return systemUserService.createSystemUser(dto);
    }

    @Operation(summary = "系统-用户更新",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PutMapping("/update")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user:update')")
    public SystemUserVO update(@RequestBody @Valid SystemUserUpdateDTO dto) {
        return systemUserService.updateSystemUser(dto);
    }

    @Operation(summary = "系统-用户删除",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @DeleteMapping("/delete/{userId}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user:delete')")
    public void delete(@PathVariable(name = "userId") Long userId) {
        systemUserService.deleteById(userId);
    }

    @Operation(summary = "系统-用户批量删除",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @DeleteMapping("/deleteByIdList/{userIdList}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user:delete')")
    public void delete(@PathVariable(name = "userIdList") List<Long> userIdList) {
        systemUserService.deleteByIdList(userIdList);
    }

    @Operation(summary = "重置用户密码",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PutMapping("/resetPassword/{id}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:user:resetPassword')")
    public void changePassword(@PathVariable(name = "id") Long id) {
        systemUserService.resetPassword(id);
    }

    @Operation(summary = "修改密码",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PostMapping("/changePassword")
    public void changePassword(@RequestBody @Valid ChangePasswordDTO changePasswordDTO) {
        systemUserService.changePassword(changePasswordDTO);
    }

    @Operation(summary = "启用|禁用系统用户",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PutMapping("/updateDisabled/{id}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:user:update')")
    public void updateDisabled(@PathVariable(name = "id") Long id) {
        systemUserService.updateDisabled(id);
    }

}
