package xin.nick.system.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.common.core.annotation.RequestLog;
import xin.nick.common.core.annotation.UnifiedResult;
import xin.nick.common.core.constant.SystemConstants;
import xin.nick.system.domain.dto.SystemUserRoleCreateDTO;
import xin.nick.system.domain.dto.SystemUserRoleUpdateDTO;
import xin.nick.system.domain.query.SystemUserRoleQuery;
import xin.nick.system.domain.vo.SystemUserRoleVO;
import xin.nick.system.service.SystemUserRoleService;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 系统-用户角色关联 前端控制器
 * </p>
 *
 * @author Nick
 * @since 2023-11-08
 */
@Tag(name = "系统-用户角色关联模块")
@UnifiedResult
@RequestLog
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/system-user-role")
public class SystemUserRoleController {

    private final SystemUserRoleService systemUserRoleService;

    @Operation(summary = "系统-用户角色关联详情",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getDetail/{id}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user-role:detail')")
    public SystemUserRoleVO getDetail(@PathVariable(name = "id") Long id) {
        return systemUserRoleService.getDetail(id);
    }

    @Operation(summary = "系统-用户角色关联列表",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getList")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user-role:list')")
    public List<SystemUserRoleVO> getList(@ParameterObject SystemUserRoleQuery query) {
        return systemUserRoleService.getList(query);
    }

    @Operation(summary = "系统-用户角色关联列表分页",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @GetMapping("/getListPage")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user-role:list')")
    public Page<SystemUserRoleVO> getListPage(@ParameterObject SystemUserRoleQuery query) {
        return systemUserRoleService.getListPage(query);
    }

    @Operation(summary = "系统-用户角色关联创建",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PostMapping("/create")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user-role:create')")
    public SystemUserRoleVO create(@RequestBody @Valid SystemUserRoleCreateDTO dto) {
        return systemUserRoleService.createSystemUserRole(dto);
    }

    @Operation(summary = "系统-用户角色关联更新",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @PutMapping("/update")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user-role:update')")
    public SystemUserRoleVO update(@RequestBody @Valid SystemUserRoleUpdateDTO dto) {
        return systemUserRoleService.updateSystemUserRole(dto);
    }

    @Operation(summary = "系统-用户角色关联删除",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user-role:delete')")
    public void delete(@PathVariable(name = "id") Long id) {
        systemUserRoleService.deleteById(id);
    }

    @Operation(summary = "系统-用户角色关联批量删除",
            security = {@SecurityRequirement(name = SystemConstants.HEADER_AUTHORIZATION)})
    @DeleteMapping("/deleteByIdList/{idList}")
    @PreAuthorize("hasRole('" + SystemConstants.ROOT + "') || hasAnyAuthority('system:system-user-role:delete')")
    public void delete(@PathVariable(name = "idList") List<Long> idList) {
        systemUserRoleService.deleteByIdList(idList);
    }

}
