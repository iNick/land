package xin.nick.system.domain.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;


/**
 * 修改密码
 *
 * @author Nick
 * @date 2022/8/10
 */
@Data
public class ChangePasswordDTO {

    @Schema(description = "密码")
    @NotBlank(message = "[password]不可为空")
    private String password;

    @Schema(description = "新密码")
    @NotBlank(message = "[newPassword]不可为空")
    private String newPassword;

}
