package xin.nick.system.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * <p>
 * 系统-权限
 * </p>
 *
 * @author Nick
 * @since 2024-02-28
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SystemAuthorityCreateDTO extends SystemAuthorityDTO {

}
