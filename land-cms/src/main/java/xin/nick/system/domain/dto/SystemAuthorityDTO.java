package xin.nick.system.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import xin.nick.system.domain.enums.MenuTypeEnum;

import javax.validation.constraints.Size;

/**
 * <p>
 * 系统-权限
 * </p>
 *
 * @author Nick
 * @since 2024-02-28
 */
@Data
public class SystemAuthorityDTO {

    /**
     * 父级id
     */
    private Long parentId;

    /**
     * 权限key
     */
    @Size(max = 255, message = "[authorityKey]不可以超过255")
    private String authorityKey;

    /**
     * 权限路径
     */
    @Size(max = 255, message = "[authorityPath]不可以超过255")
    private String authorityPath;

    /**
     * 菜单名字
     */
    @Size(max = 255, message = "[menuName]不可以超过255")
    private String menuName;

    /**
     * 菜单类型
     */
    @Size(max = 255, message = "[menuType]不可以超过255")
    private MenuTypeEnum menuType;

    /**
     * 菜单元数据
     */
    private String menuMeta;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer sort;

    /**
     * 备注
     */
    @Size(max = 255, message = "[remark]不可以超过255")
    private String remark;


    /**
     * 是否禁用(1是,0否)
     */
    private Boolean disabled;


}
