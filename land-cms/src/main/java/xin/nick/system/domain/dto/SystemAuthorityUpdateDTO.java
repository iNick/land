package xin.nick.system.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;


/**
 * <p>
 * 系统-权限
 * </p>
 *
 * @author Nick
 * @since 2024-02-28
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SystemAuthorityUpdateDTO extends SystemAuthorityDTO  {

    @Schema(description = "权限id")
    @NotNull(message="[authorityId]不可为空")
    private Long authorityId;

}
