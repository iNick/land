package xin.nick.system.domain.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统配置
 * </p>
 *
 * @author Nick
 * @since 2023-11-28
 */
@Data
public class SystemConfigCreateDTO {

    /**
     * 配置id
     */
    @NotNull(message="[configId]不可为空")
    private Long configId;

    /**
     * 配置Name
     */
    @Size(max = 255, message = "[configKey]不可以超过255")
    private String configName;

    /**
     * 配置key
     */
    @Size(max = 255, message = "[configKey]不可以超过255")
    private String configKey;

    /**
     * 配置value
     */
    @Size(max = 255, message = "[configValue]不可以超过255")
    private String configValue;

    /**
     * 配置json
     */
    private String configJson;

    /**
     * 备注
     */
    @Size(max = 255, message = "[remark]不可以超过255")
    private String remark;

    /**
     * 版本号
     */
    private Long version;

    /**
     * 是否禁用(1是,0否)
     */
    private Boolean disabled;

    /**
     * 是否删除(1是,0否)
     */
    private Boolean deleted;


}
