package xin.nick.system.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 系统-角色权限关联
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Data
@Schema(description = "SystemRoleAuthorityDTO对象-系统-角色权限关联")
public class SystemRoleAuthorityDTO {

    @Schema(description = "角色权限id")
    @NotNull(message = "[id]不可为空")
    private Long id;

    @Schema(description = "角色id")
    @NotNull(message = "[roleId]不可为空")
    private Long roleId;

    @Schema(description = "权限id")
    @NotNull(message = "[authorityId]不可为空")
    private Long authorityId;


}
