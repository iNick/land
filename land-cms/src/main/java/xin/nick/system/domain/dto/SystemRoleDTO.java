package xin.nick.system.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <p>
 * 系统-角色
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Data
@Schema(description = "SystemRoleDTO对象-系统-角色")
public class SystemRoleDTO {

    @Schema(description = "角色id")
    @NotNull(message = "[roleId]不可为空")
    private Long roleId;

    @Schema(description = "角色key")
    @Size(max = 100, message = "[roleKey]不可以超过100")
    private String roleKey;

    @Schema(description = "角色名字")
    @Size(max = 100, message = "[roleName]不可以超过100")
    private String roleName;

    @Schema(description = "备注")
    @Size(max = 255, message = "[remark]不可以超过255")
    private String remark;

    @Schema(description = "版本号")
    private Long version;

    @Schema(description = "是否禁用(1是,0否)")
    private Boolean disabled;

    @Schema(description = "是否删除(1是,0否)")
    private Boolean idDeleted;


}
