package xin.nick.system.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <p>
 * 系统-用户
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Data
@Schema(description = "SystemUserCreateDTO对象-系统-用户")
public class SystemUserCreateDTO {

    @Schema(description = "系统用户名字(账号)")
    @NotBlank(message = "[username]不可为空")
    @Size(max = 100, message = "[username]不可以超过100")
    private String username;

    @Schema(description = "系统用户密码")
    @NotBlank(message = "[password]不可为空")
    @Size(max = 100, message = "[password]不可以超过100")
    private String password;

    @Schema(description = "系统用户昵称")
    @Size(max = 100, message = "[nickname]不可以超过100")
    private String nickname;

    @Schema(description = "系统用户手机号")
    @Size(max = 32, message = "[telNumber]不可以超过32")
    private String telNumber;

    @Schema(description = "备注")
    @Size(max = 255, message = "[remark]不可以超过255")
    private String remark;

    @Schema(description = "所属角色的id列表")
    private List<Long> roleIdList;

    @Schema(description = "版本号")
    private Long version;

    @Schema(description = "是否禁用(1是,0否)")
    private Boolean disabled;

    @Schema(description = "是否删除(1是,0否)")
    private Boolean deleted;


}
