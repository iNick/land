package xin.nick.system.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 系统-用户角色关联
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Data
@Schema(description = "SystemUserRoleCreateDTO对象-系统-用户角色关联")
public class SystemUserRoleCreateDTO {

    @Schema(description = "用户id")
    @NotNull(message = "[userId]不可为空")
    private Long userId;

    @Schema(description = "角色id")
    @NotNull(message = "[roleId]不可为空")
    private Long roleId;


}
