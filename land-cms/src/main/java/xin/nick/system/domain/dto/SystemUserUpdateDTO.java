package xin.nick.system.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;


/**
 * <p>
 * 系统用户信息
 * </p>
 *
 * @author Nick
 * @since 2023-01-03
 */
@Data
public class SystemUserUpdateDTO {

    @Schema(description = "系统用户id")
    @NotNull(message = "[userId]不可为空")
    private Long userId;

    @Schema(description = "系统用户名字")
    @Size(max = 100, message = "[username]不可以超过100")
    private String username;

    @Schema(description = "系统用户密码")
    @Size(max = 100, message = "[password]不可以超过100")
    private String password;

    @Schema(description = "系统用户昵称")
    @Size(max = 100, message = "[nickname]不可以超过100")
    private String nickname;

    @Schema(description = "系统用户手机号")
    @Size(max = 32, message = "[telNumber]不可以超过32")
    private String telNumber;

    @Schema(description = "所属角色的id列表")
    private List<Long> roleIdList;

    @Schema(description = "备注")
    @Size(max = 255, message = "[remark]不可以超过255")
    private String remark;


}
