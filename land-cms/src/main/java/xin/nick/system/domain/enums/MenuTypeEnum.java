package xin.nick.system.domain.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 菜单类型
 * </p>
 *
 * @author Nick
 * @since 2023-01-03
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum MenuTypeEnum implements IEnum<String> {

    DIRECTORY("DIRECTORY", "目录"),
    MENU("MENU", "菜单"),
    BUTTON("BUTTON", "按钮"),
    EXTERNAL("EXTERNAL", "外部"),
    ;

    private String value;
    private String name;
}
