package xin.nick.system.domain.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 菜单类型
 * </p>
 *
 * @author Nick
 * @since 2023-01-03
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum SystemConfigKeyEnum implements IEnum<String> {

    DEFAULT_CONFIG_KEY("DEFAULT_CONFIG_KEY", "默认配置"),

    ;

    private String value;
    private String name;
}
