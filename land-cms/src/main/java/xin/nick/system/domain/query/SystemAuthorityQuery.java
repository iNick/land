package xin.nick.system.domain.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import xin.nick.common.core.query.PageQuery;
import xin.nick.system.domain.enums.MenuTypeEnum;


/**
 * <p>
 * 系统-权限
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Data
@Schema(description = "SystemAuthorityQuery对象-系统-权限")
public class SystemAuthorityQuery extends PageQuery {

    @Schema(description = "id")
    private Long authorityId;

    @Schema(description = "父级id")
    private Long parentId;

    @Schema(description = "权限key")
    private String authorityKey;

    @Schema(description = "权限路径")
    private String authorityPath;

    @Schema(description = "菜单名字")
    private String menuName;

    @Schema(description = "菜单类型")
    private MenuTypeEnum menuType;

    @Schema(description = "菜单元数据")
    private String menuMeta;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "是否禁用(1是,0否)")
    private Boolean disabled;
}
