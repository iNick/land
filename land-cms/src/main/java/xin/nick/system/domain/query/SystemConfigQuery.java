package xin.nick.system.domain.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import xin.nick.common.core.query.PageQuery;

import javax.validation.constraints.Size;


/**
 * <p>
 * 系统配置
 * </p>
 *
 * @author Nick
 * @since 2023-11-28
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SystemConfigQuery extends PageQuery{


    @Schema(description = "配置key")
    private String configKey;

    @Schema(description = "配置Name")
    private String configName;

    @Schema(description = "配置value")
    private String configValue;

    @Schema(description = "配置json")
    private String configJson;

    @Schema(description = "备注")
    private String remark;

}
