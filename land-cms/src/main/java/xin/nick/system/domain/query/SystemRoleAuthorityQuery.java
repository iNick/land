package xin.nick.system.domain.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import xin.nick.common.core.query.PageQuery;


/**
 * <p>
 * 系统-角色权限关联
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Data
@Schema(description = "SystemRoleAuthorityQuery对象-系统-角色权限关联")
public class SystemRoleAuthorityQuery extends PageQuery {

}
