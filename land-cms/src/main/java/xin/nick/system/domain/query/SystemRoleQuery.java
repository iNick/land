package xin.nick.system.domain.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import xin.nick.common.core.query.PageQuery;


/**
 * <p>
 * 系统-角色
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Data
@Schema(description = "SystemRoleQuery对象-系统-角色")
public class SystemRoleQuery extends PageQuery {

    @Schema(description = "角色id")
    private Long roleId;

    @Schema(description = "角色key")
    private String roleKey;

    @Schema(description = "角色名")
    private String roleName;

    @Schema(description = "禁用状态")
    private Boolean disabled;

}
