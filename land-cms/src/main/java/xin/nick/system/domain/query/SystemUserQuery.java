package xin.nick.system.domain.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import xin.nick.common.core.query.PageQuery;


/**
 * <p>
 * 系统用户信息
 * </p>
 *
 * @author Nick
 * @since 2023-01-03
 */
@Data
@Schema(description = "SystemUserQuery对象-系统用户信息")
public class SystemUserQuery extends PageQuery {

    @Schema(description = "系统用户id")
    private Long userId;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "昵称")
    private String nickname;

    @Schema(description = "手机号")
    private String telNumber;

    @Schema(description = "是否禁用")
    private Boolean disabled;

}
