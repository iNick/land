package xin.nick.system.domain.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import xin.nick.common.core.query.PageQuery;


/**
 * <p>
 * 系统-用户角色关联
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Data
@Schema(description = "SystemUserRoleQuery对象-系统-用户角色关联")
public class SystemUserRoleQuery extends PageQuery {

}
