package xin.nick.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author Nick
 * @since 2023/4/30
 */
@Data
public class CurrentUserInfoVO {

    @Schema(description = "系统用户id")
    private Long userId;

    @Schema(description = "系统用户名字")
    private String username;

    @Schema(description = "系统用户昵称")
    private String nickname;

    @Schema(description = "系统用户手机号")
    private String telNumber;

    @Schema(description = "备注")
    private String remark;
}
