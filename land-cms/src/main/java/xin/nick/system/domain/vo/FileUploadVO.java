package xin.nick.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author Nick
 * @date 2022/8/20
 */
@Data
public class FileUploadVO {

    @Schema(description = "fileUrl")
    private String fileUrl;

    @Schema(description = "fileUri")
    private String fileUri;

}

