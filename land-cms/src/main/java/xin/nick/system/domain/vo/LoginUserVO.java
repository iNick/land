package xin.nick.system.domain.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 登录成功后用户vo
 *
 * @author Nick
 * @since 2022/7/27/027
 */
@Data
public class LoginUserVO {

    @Schema(description = "用户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @Schema(description = "昵称")
    private String nickname;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "token")
    private String token;

    // 2023年1月14日 角色和权限暂时不通过登录返回
//    @Schema(description = "权限列表")
//    private List<AuthorityVO> authorityList;
//
//    @Schema(description = "角色列表")
//    private List<RoleVO> roleList;

}
