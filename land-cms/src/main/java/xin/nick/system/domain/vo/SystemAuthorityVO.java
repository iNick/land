package xin.nick.system.domain.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import xin.nick.system.domain.enums.MenuTypeEnum;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 系统-权限 VO
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Data
@Schema(description = "SystemAuthorityVO对象-系统-权限")
public class SystemAuthorityVO {

    @Schema(description = "权限id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long authorityId;

    @Schema(description = "父级id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    @Schema(description = "子级列表")
    private List<SystemAuthorityVO> children = new ArrayList<>();

    @Schema(description = "权限key")
    private String authorityKey;

    @Schema(description = "权限路径")
    private String authorityPath;

    @Schema(description = "菜单名字")
    private String menuName;

    @Schema(description = "菜单类型")
    private MenuTypeEnum menuType;

    @Schema(description = "菜单元数据")
    private String menuMeta;

    @Schema(description = "排序")
    private Integer sort;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "是否禁用(1是,0否)")
    private Boolean disabled;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

}
