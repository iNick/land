package xin.nick.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 系统配置 VO
 * </p>
 *
 * @author Nick
 * @since 2023-11-28
 */
@Data
public class SystemConfigVO {

    @Schema(description = "配置id")
    private Long configId;

    @Schema(description = "配置Name")
    private String configName;

    @Schema(description = "配置key")
    private String configKey;

    @Schema(description = "配置value")
    private String configValue;

    @Schema(description = "配置json")
    private String configJson;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "版本号")
    private Long version;

    @Schema(description = "是否禁用(1是,0否)")
    private Boolean disabled;

    @Schema(description = "是否删除(1是,0否)")
    private Boolean deleted;


}
