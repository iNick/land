package xin.nick.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * 系统-角色 VO
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Data
@Schema(description = "SystemRoleVO对象-系统-角色")
public class SystemRoleVO {

    @Schema(description = "角色id")
    private Long roleId;

    @Schema(description = "角色key")
    private String roleKey;

    @Schema(description = "角色名字")
    private String roleName;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "是否禁用(1是,0否)")
    private Boolean disabled;

    @Schema(description = "权限列表")
    private List<SystemAuthorityVO> authorityList;


}
