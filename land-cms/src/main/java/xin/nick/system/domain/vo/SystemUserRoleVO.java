package xin.nick.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * <p>
 * 系统-用户角色关联 VO
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Data
@Schema(description = "SystemUserRoleVO对象-系统-用户角色关联")
public class SystemUserRoleVO {

    @Schema(description = "用户角色id")
    private Long id;

    @Schema(description = "用户id")
    private Long userId;

    @Schema(description = "角色id")
    private Long roleId;


}
