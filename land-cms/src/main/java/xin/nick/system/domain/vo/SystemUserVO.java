package xin.nick.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * 系统用户信息 VO
 * </p>
 *
 * @author Nick
 * @since 2023-01-03
 */
@Data
@Schema(description = "SystemUserVO对象-系统用户信息")
public class SystemUserVO {

    @Schema(description = "系统用户id")
    private Long userId;

    @Schema(description = "系统用户名字")
    private String username;

    @Schema(description = "系统用户昵称")
    private String nickname;

    @Schema(description = "系统用户手机号")
    private String telNumber;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "是否禁用(1是,0否)")
    private Boolean disabled;

    @Schema(description = "所属角色列表")
    private List<SystemRoleVO> roleList;


}
