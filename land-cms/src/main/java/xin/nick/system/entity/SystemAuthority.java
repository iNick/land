package xin.nick.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import xin.nick.common.core.entity.BaseEntity;
import xin.nick.system.domain.enums.MenuTypeEnum;

/**
 * <p>
 * 系统-权限
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("system_authority")
@Schema(description = "SystemAuthority对象-系统-权限")
public class SystemAuthority extends BaseEntity {

    @Schema(description = "权限id")
    @TableId(value = "authority_id", type = IdType.AUTO)
    private Long authorityId;

    @Schema(description = "权限key")
    @TableField("authority_key")
    private String authorityKey;

    @Schema(description = "父级id")
    @TableField("parent_id")
    private Long parentId;

    @Schema(description = "权限路径")
    @TableField("authority_path")
    private String authorityPath;

    @Schema(description = "菜单名字")
    @TableField("menu_name")
    private String menuName;

    @Schema(description = "菜单类型")
    @TableField("menu_type")
    private MenuTypeEnum menuType;

    @Schema(description = "菜单元数据")
    @TableField("menu_meta")
    private String menuMeta;

    // 越小越靠前
    @Schema(description = "排序")
    @TableField("sort")
    private Integer sort;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;

    @Schema(description = "版本号")
    @TableField("version")
    @Version
    private Long version;

    @Schema(description = "是否禁用(1是,0否)")
    @TableField("disabled")
    private Boolean disabled;

    @Schema(description = "是否删除(1是,0否)")
    @TableField("deleted")
    @TableLogic
    private Boolean deleted;


}
