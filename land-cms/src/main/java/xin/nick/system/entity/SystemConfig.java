package xin.nick.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import xin.nick.common.core.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import xin.nick.system.domain.enums.SystemConfigKeyEnum;

import java.time.LocalDateTime;

/**
 * <p>
 * 系统配置
 * </p>
 *
 * @author Nick
 * @since 2023-11-28
 */
@Getter
@Setter
@TableName("system_config")
@Schema(name = "SystemConfig", description = "系统配置")
public class SystemConfig extends BaseEntity {

    @Schema(description = "配置id")
    @TableId("config_id")
    private Long configId;

    @Schema(description = "配置name")
    @TableField("config_name")
    private String configName;

    // 期望使用枚举 SystemConfigKeyEnum 统一key值
    // 但是这里存入数据库需要兼容String类型的key操作
    @Schema(description = "配置key")
    @TableField("config_key")
    private String configKey;

    @Schema(description = "配置value")
    @TableField("config_value")
    private String configValue;

    @Schema(description = "配置json")
    @TableField("config_json")
    private String configJson;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;

    @Schema(description = "版本号")
    @TableField("version")
    @Version
    private Long version;

    @Schema(description = "是否禁用(1是,0否)")
    @TableField("disabled")
    private Boolean disabled;

    @Schema(description = "是否删除(1是,0否)")
    @TableField("deleted")
    @TableLogic
    private Boolean deleted;


}
