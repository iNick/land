package xin.nick.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import xin.nick.common.core.entity.BaseEntity;

/**
 * <p>
 * 系统-角色
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Getter
@Setter
@TableName("system_role")
@Schema(description = "SystemRole对象-系统-角色")
public class SystemRole extends BaseEntity {

    @Schema(description = "角色id")
    @TableId(value = "role_id", type = IdType.AUTO)
    private Long roleId;

    @Schema(description = "角色key")
    @TableField("role_key")
    private String roleKey;

    @Schema(description = "角色名字")
    @TableField("role_name")
    private String roleName;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;

    @Schema(description = "版本号")
    @TableField("version")
    @Version
    private Long version;

    @Schema(description = "是否禁用(1是,0否)")
    @TableField("disabled")
    private Boolean disabled;

    @Schema(description = "是否删除(1是,0否)")
    @TableField("deleted")
    private Boolean deleted;


}
