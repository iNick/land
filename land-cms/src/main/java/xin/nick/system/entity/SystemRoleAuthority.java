package xin.nick.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 系统-角色权限关联
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Getter
@Setter
@TableName("system_role_authority")
@Schema(description = "SystemRoleAuthority对象-系统-角色权限关联")
public class SystemRoleAuthority {

    @Schema(description = "角色权限id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(description = "角色id")
    @TableField("role_id")
    private Long roleId;

    @Schema(description = "权限id")
    @TableField("authority_id")
    private Long authorityId;


}
