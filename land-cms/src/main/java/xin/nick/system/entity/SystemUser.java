package xin.nick.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import xin.nick.common.core.entity.BaseEntity;

/**
 * <p>
 * 系统-用户
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Getter
@Setter
@TableName("system_user")
@Schema(description = "SystemUser对象-系统-用户")
public class SystemUser extends BaseEntity {

    @Schema(description = "系统用户id")
    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    @Schema(description = "系统用户名字(账号)")
    @TableField("username")
    private String username;

    @Schema(description = "系统用户密码")
    @TableField("password")
    private String password;

    @Schema(description = "系统用户昵称")
    @TableField("nickname")
    private String nickname;

    @Schema(description = "系统用户手机号")
    @TableField("tel_number")
    private String telNumber;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;

    @Schema(description = "版本号")
    @TableField("version")
    @Version
    private Long version;

    @Schema(description = "是否禁用(1是,0否)")
    @TableField("disabled")
    private Boolean disabled;

    @Schema(description = "是否删除(1是,0否)")
    @TableField("deleted")
    @TableLogic
    private Boolean deleted;


}
