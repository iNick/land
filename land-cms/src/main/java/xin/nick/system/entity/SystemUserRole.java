package xin.nick.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 系统-用户角色关联
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Getter
@Setter
@TableName("system_user_role")
@Schema(description = "SystemUserRole对象-系统-用户角色关联")
public class SystemUserRole {

    @Schema(description = "用户角色id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(description = "用户id")
    @TableField("user_id")
    private Long userId;

    @Schema(description = "角色id")
    @TableField("role_id")
    private Long roleId;


}
