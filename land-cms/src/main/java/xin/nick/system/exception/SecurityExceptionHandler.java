package xin.nick.system.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import xin.nick.common.core.constant.SystemConstants;
import xin.nick.common.core.entity.Result;
import xin.nick.common.core.entity.ResultCode;

/**
 * Security 异常处理
 *
 * @author Nick
 * @since 2023/5/30
 */
@Slf4j
@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SecurityExceptionHandler {


    /**
     * 登录认证异常
     */
    @ExceptionHandler(InsufficientAuthenticationException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Result<String> handleInsufficientAuthenticationException(InsufficientAuthenticationException e) {
        log.warn("登录认证异常: {}", e.getMessage());
        return Result.custom(ResultCode.UNAUTHORIZED.getValue(), e.getMessage());
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Result<String> handleAuthenticationException(AuthenticationException e) {
        log.warn("身份验证异常: {}", e.getMessage());
        return Result.custom(ResultCode.UNAUTHORIZED.getValue(), e.getMessage());
    }


    @ExceptionHandler(InternalAuthenticationServiceException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Result<String> handleInternalAuthenticationServiceException(InternalAuthenticationServiceException e) {
        log.error("内部鉴权服务异常", e);
        return Result.custom(ResultCode.UNAUTHORIZED.getValue(), e.getMessage());
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result<String> handleBadCredentialsException(BadCredentialsException e) {
        log.warn("校验凭证错误: {}", e.getMessage());
        return Result.custom(SystemConstants.REQUEST_PARAM_ERROR, e.getMessage());
    }

    /**
     * AccessDeniedException
     */
    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Result<String> handleAccessDeniedException(AccessDeniedException e) {
        log.warn("没有权限: {}", e.getMessage());
        return Result.custom(ResultCode.FORBIDDEN, e.getMessage());
    }

}
