package xin.nick.system.filter;


import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import com.alibaba.fastjson2.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jose4j.jwt.JwtClaims;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import xin.nick.common.core.constant.SystemConstants;
import xin.nick.common.core.util.IpUtil;
import xin.nick.common.core.util.JwtUtil;
import xin.nick.common.core.util.QueryUtil;
import xin.nick.system.config.ApplicationSystemProperties;
import xin.nick.system.entity.SystemUser;
import xin.nick.system.manager.SystemUserManager;
import xin.nick.system.mapper.SystemUserMapper;
import xin.nick.system.service.MyUserDetailsService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * token过滤器 验证token有效性
 *
 * @author Nick
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class TokenAuthenticationTokenFilter extends OncePerRequestFilter {

    private final RedisTemplate<String, Object> redisTemplate;
    private final ApplicationSystemProperties applicationSystemProperties;
    private final SystemUserManager systemUserManager;
    private final SystemUserMapper systemUserMapper;
    private final MyUserDetailsService myUserDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        log.debug("请求头: {}", JSON.toJSONString(request.getHeaderNames()));
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headName = headerNames.nextElement();
            Enumeration<String> headerValueList = request.getHeaders(headName);
            while (headerValueList.hasMoreElements()) {
                String headerValue = headerValueList.nextElement();
                log.debug("{}: {}", headName, headerValue);
            }
        }

        String token = QueryUtil.getToken(request);

        // 当使用session的时候,会自动从session中获取用户信息,则不会进入token校验逻辑
        // 这里尝试从 context 中获取登录用户信息
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (Objects.isNull(authentication)) {
            // 检测token并获取userId
            Long userId = checkTokenAndGetUserId(token);
            if (Objects.nonNull(userId)) {

                // 获取用户信息
                SystemUser systemUser = systemUserMapper.selectById(userId);
                if (Objects.nonNull(systemUser)) {
                    // 根据用户获取权限信息
                    List<SimpleGrantedAuthority> authorityList = myUserDetailsService.getAuthorityList(userId);
                    // 登录用户信息
                    UserDetails user = new User(systemUser.getUsername(), systemUser.getPassword(), authorityList);
                    // 将创建用户信息放入 context
                    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user, systemUser.getPassword(), authorityList);
                    authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                }

            }

        }
        // 输出请求情况

        log.info("tokenFilter: ip: {}, uri: {}, token: {}",
                IpUtil.getIpAddress(request),
                request.getRequestURI(),
                token);


        // 往下一个过滤器走
        chain.doFilter(request, response);

    }

    /**
     * 检查token信息,并获取token携带的userId
     *
     * @param token token
     * @return Long
     */
    public Long checkTokenAndGetUserId(String token) {

        // 用户未登录
        if (StrUtil.isBlank(token)) {
            return null;
        }

        try {
            // 通过jwt获取本次登录的 userId
            JwtClaims jwtClaims = JwtUtil.getJwtClaims(token, applicationSystemProperties.getJwtSecret());
            String subject = jwtClaims.getSubject();
            AES aes = SecureUtil.aes(applicationSystemProperties.getAesSecret().getBytes());
            String userIdString = aes.decryptStr(subject);

            // 检查Redis中token和本次的是否相同
            String tokenKey = SystemConstants.USER_TOKEN_KEY + userIdString;
            Object redisToken = redisTemplate.opsForValue().get(tokenKey);
            if (Objects.isNull(redisToken)) {
                return null;
            }
            String redisTokenString = String.valueOf(redisToken);
            if (StrUtil.equals(token, redisTokenString)) {
                // 相同则重置用户id过期时间,并返回
                redisTemplate.expire(tokenKey, SystemConstants.USER_TOKEN_EXPIRE, TimeUnit.SECONDS);
                return Long.parseLong(userIdString);

            }

        } catch (Exception e) {
            log.error("登录token校验异常: {}", e.getMessage());
        }

        return null;
    }



}
