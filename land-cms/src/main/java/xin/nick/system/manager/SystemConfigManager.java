package xin.nick.system.manager;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import xin.nick.common.core.constant.SystemConstants;
import xin.nick.system.domain.enums.SystemConfigKeyEnum;
import xin.nick.system.entity.SystemConfig;
import xin.nick.system.mapper.SystemConfigMapper;

import java.util.Objects;

/**
 * @author Nick
 * @since 2023/11/28
 */
@Component
@RequiredArgsConstructor
public class SystemConfigManager {

    private final SystemConfigMapper systemConfigMapper;

    /**
     * 设置配置Json字符串
     * @param systemConfigKey SystemConfigKeyEnum
     * @param jsonString  String
     * @return String
     */
    public String setConfigJsonString(SystemConfigKeyEnum systemConfigKey, String jsonString) {
        if (Objects.isNull(systemConfigKey)) {
            return null;
        }
        return setConfigJsonString(systemConfigKey.getValue(), jsonString);
    }

    /**
     * 设置配置Json字符串
     * @param systemConfigKey String
     * @param jsonString String
     * @return String
     */
    public String setConfigJsonString(String systemConfigKey, String jsonString) {
        // 判断数据库是否有
        SystemConfig systemConfig = systemConfigMapper.selectOne(
                Wrappers.<SystemConfig>lambdaQuery()
                        .eq(SystemConfig::getConfigKey, systemConfigKey).last(SystemConstants.LIMIT_1));

        // 没有则新增
        if (Objects.isNull(systemConfig)) {
            systemConfig = new SystemConfig();
            systemConfig.setConfigKey(systemConfigKey);
            systemConfig.setConfigJson(jsonString);
            systemConfigMapper.insert(systemConfig);
        } else {
            // 有则更新
            systemConfigMapper.update(null,
                    Wrappers.<SystemConfig>lambdaUpdate()
                            .eq(SystemConfig::getConfigKey, systemConfigKey)
                            .set(SystemConfig::getConfigJson, jsonString)
            );
        }

        // 从数据库获取新的配置信息
        return getConfigJsonString(systemConfigKey);
    }

    /**
     * 更新JSON配置
     * @param systemConfigKey String
     * @param jsonString SystemConfigKeyEnum
     * @return JSONObject
     */
    public JSONObject setConfigJson(SystemConfigKeyEnum systemConfigKey, String jsonString) {
        if (Objects.isNull(systemConfigKey)) {
            return null;
        }
        // 将对象装换之后返回
        return setConfigJson(systemConfigKey.getValue(), jsonString);
    }


    /**
     * 更新JSON配置
     * @param systemConfigKey
     * @param jsonObject
     * @return
     */
    public JSONObject setConfigJson(SystemConfigKeyEnum systemConfigKey, JSONObject jsonObject) {
        if (Objects.isNull(jsonObject)) {
            return null;
        }

        return setConfigJson(systemConfigKey.getValue(), jsonObject.toJSONString());
    }

    /**
     * 更新JSON配置
     * @param systemConfigKey
     * @param jsonString
     * @return
     */
    public JSONObject setConfigJson(String systemConfigKey, String jsonString) {
        // 判断数据库是否有
        SystemConfig systemConfig = systemConfigMapper.selectOne(
                Wrappers.<SystemConfig>lambdaQuery()
                        .eq(SystemConfig::getConfigKey, systemConfigKey).last(SystemConstants.LIMIT_1));

        // 没有则新增
        if (Objects.isNull(systemConfig)) {
            systemConfig = new SystemConfig();
            systemConfig.setConfigKey(systemConfigKey);
            systemConfig.setConfigJson(jsonString);
            systemConfigMapper.insert(systemConfig);
        } else {
            // 有则更新
            systemConfigMapper.update(null,
                    Wrappers.<SystemConfig>lambdaUpdate()
                            .eq(SystemConfig::getConfigKey, systemConfigKey)
                            .set(SystemConfig::getConfigJson, jsonString)
            );
        }

        // 从数据库获取新的配置信息
        return getConfigJson(systemConfigKey);
    }

    /**
     * 更新指定类型的配置
     *
     * @param systemConfigKey
     * @param jsonClass
     * @param object
     * @param <T>
     * @return
     */
    public <T> T setConfigObject(SystemConfigKeyEnum systemConfigKey, Class<T> jsonClass, Object object) {

        if (Objects.isNull(systemConfigKey)) {
            return null;
        }

        String jsonString = JSON.toJSONString(object);

        setConfigJson(systemConfigKey, jsonString);

        // 将对象装换之后返回
        return getConfigObject(systemConfigKey, jsonClass);
    }

    /**
     * 更新指定类型的配置
     *
     * @param systemConfigKey
     * @param jsonClass
     * @param object
     * @param <T>
     * @return
     */
    public <T> T setConfigObject(String systemConfigKey, Class<T> jsonClass, Object object) {

        String jsonString = JSON.toJSONString(object);

        setConfigJson(systemConfigKey, jsonString);

        // 将对象装换之后返回
        return getConfigObject(systemConfigKey, jsonClass);
    }

    /**
     * 获取配置json信息
     *
     * @param systemConfigKey
     * @return
     */
    public JSONObject getConfigJson(SystemConfigKeyEnum systemConfigKey) {
        if (Objects.isNull(systemConfigKey)) {
            return null;
        }
        return getConfigJson(systemConfigKey.getValue());
    }

    /**
     * 获取配置json信息
     *
     * @param systemConfigKey
     * @return
     */
    public JSONObject getConfigJson(String systemConfigKey) {

        SystemConfig systemConfig = systemConfigMapper.selectOne(
                Wrappers.<SystemConfig>lambdaQuery()
                        .eq(SystemConfig::getConfigKey, systemConfigKey).last(SystemConstants.LIMIT_1));
        if (Objects.isNull(systemConfig)) {
            return null;
        }
        return JSON.parseObject(systemConfig.getConfigJson());
    }

    /**
     * 获取配置json字符串
     *
     * @param systemConfigKey
     * @return
     */
    public String getConfigJsonString(SystemConfigKeyEnum systemConfigKey) {
        if (Objects.isNull(systemConfigKey)) {
            return null;
        }

        SystemConfig systemConfig = systemConfigMapper.selectOne(
                Wrappers.<SystemConfig>lambdaQuery()
                        .eq(SystemConfig::getConfigKey, systemConfigKey).last(SystemConstants.LIMIT_1));
        if (Objects.isNull(systemConfig)) {
            return null;
        }
        return systemConfig.getConfigJson();
    }

    /**
     * 获取配置json字符串
     *
     * @param systemConfigKey
     * @return
     */
    public String getConfigJsonString(String systemConfigKey) {

        SystemConfig systemConfig = systemConfigMapper.selectOne(
                Wrappers.<SystemConfig>lambdaQuery()
                        .eq(SystemConfig::getConfigKey, systemConfigKey).last(SystemConstants.LIMIT_1));
        if (Objects.isNull(systemConfig)) {
            return null;
        }
        return systemConfig.getConfigJson();
    }

    /**
     * 获取配置json信息,指定类型返回
     *
     * @param systemConfigKey
     * @param jsonClass
     * @param <T>
     * @return
     */
    public <T> T getConfigObject(SystemConfigKeyEnum systemConfigKey, Class<T> jsonClass) {
        if (Objects.isNull(systemConfigKey)) {
            return null;
        }
        return getConfigObject(systemConfigKey.getValue(), jsonClass);
    }

    /**
     * 获取配置对象信息,指定类型返回
     *
     * @param systemConfigKey
     * @param objectClass
     * @param <T>
     * @return
     */
    public <T> T getConfigObject(String systemConfigKey, Class<T> objectClass) {
        SystemConfig systemConfig = systemConfigMapper.selectOne(
                Wrappers.<SystemConfig>lambdaQuery()
                        .eq(SystemConfig::getConfigKey, systemConfigKey).last(SystemConstants.LIMIT_1));
        if (Objects.nonNull(systemConfig)) {
            return JSON.parseObject(systemConfig.getConfigJson(), objectClass);
        }

        return null;

    }


    /**
     * 获取配置的String
     *
     * @param systemConfigKey
     * @return
     */
    public String getConfigString(SystemConfigKeyEnum systemConfigKey) {
        if (Objects.isNull(systemConfigKey)) {
            return null;
        }
        return getConfigString(systemConfigKey.getValue());
    }

    /**
     * 获取配置的String
     *
     * @param systemConfigKey
     * @return
     */
    public String getConfigString(String systemConfigKey) {
        SystemConfig systemConfig = systemConfigMapper.selectOne(
                Wrappers.<SystemConfig>lambdaQuery()
                        .eq(SystemConfig::getConfigKey, systemConfigKey).last(" limit 1 "));
        if (Objects.nonNull(systemConfig)) {
            return systemConfig.getConfigValue();
        }
        return null;
    }

    /**
     * 设置配置String
     *
     * @param systemConfigKey
     * @param configValue
     * @return
     */
    public String setConfigString(SystemConfigKeyEnum systemConfigKey, String configValue) {
        if (Objects.isNull(systemConfigKey)) {
            return null;
        }
        return setConfigString(systemConfigKey.getValue(), configValue);
    }

    /**
     * 设置配置String
     *
     * @param systemConfigKey
     * @param configValue
     * @return
     */
    public String setConfigString(String systemConfigKey, String configValue) {

        // 判断数据库是否有
        SystemConfig systemConfig = systemConfigMapper.selectOne(
                Wrappers.<SystemConfig>lambdaQuery()
                        .eq(SystemConfig::getConfigKey, systemConfigKey).last(" limit 1 "));

        // 没有则新增
        if (Objects.isNull(systemConfig)) {
            systemConfig = new SystemConfig();
            systemConfig.setConfigKey(systemConfigKey);
            systemConfig.setConfigValue(configValue);
            systemConfigMapper.insert(systemConfig);
        } else {
            // 有则更新
            systemConfigMapper.update(null,
                    Wrappers.<SystemConfig>lambdaUpdate()
                            .eq(SystemConfig::getConfigKey, systemConfigKey)
                            .set(SystemConfig::getConfigValue, configValue)
            );
        }

        return getConfigString(systemConfigKey);
    }
}
