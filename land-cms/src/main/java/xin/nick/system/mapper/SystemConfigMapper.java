package xin.nick.system.mapper;

import xin.nick.system.entity.SystemConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统配置 Mapper 接口
 * </p>
 *
 * @author Nick
 * @since 2023-11-28
 */
@Mapper
public interface SystemConfigMapper extends BaseMapper<SystemConfig> {

}
