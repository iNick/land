package xin.nick.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xin.nick.system.entity.SystemRoleAuthority;

/**
 * <p>
 * 系统-角色权限关联 Mapper 接口
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Mapper
public interface SystemRoleAuthorityMapper extends BaseMapper<SystemRoleAuthority> {

}
