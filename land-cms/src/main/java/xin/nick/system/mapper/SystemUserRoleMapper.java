package xin.nick.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xin.nick.system.entity.SystemUserRole;

/**
 * <p>
 * 系统-用户角色关联 Mapper 接口
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Mapper
public interface SystemUserRoleMapper extends BaseMapper<SystemUserRole> {

}
