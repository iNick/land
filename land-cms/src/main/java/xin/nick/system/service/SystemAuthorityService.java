package xin.nick.system.service;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xin.nick.common.core.util.BeanCopierUtil;
import xin.nick.system.domain.dto.SystemAuthorityCreateDTO;
import xin.nick.system.domain.dto.SystemAuthorityUpdateDTO;
import xin.nick.system.domain.query.SystemAuthorityQuery;
import xin.nick.system.domain.vo.SystemAuthorityVO;
import xin.nick.system.entity.SystemAuthority;
import xin.nick.system.mapper.SystemAuthorityMapper;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * <p>
 * 系统-权限 服务实现类
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Service
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class SystemAuthorityService extends ServiceImpl<SystemAuthorityMapper, SystemAuthority> {


    /**
     * 获取系统-权限详情
     *
     * @param authorityId Long
     * @return SystemAuthorityVO
     */
    public SystemAuthorityVO getDetail(Long authorityId) {
        Assert.notNull(authorityId, "[authorityId]不可为空");
        SystemAuthority systemAuthority = getById(authorityId);
        Assert.notNull(systemAuthority, "系统-权限不存在");
        return BeanCopierUtil.copyProperties(systemAuthority, SystemAuthorityVO.class);
    }

    /**
     * 获取系统-权限列表
     *
     * @param query SystemAuthorityQuery
     * @return List<SystemAuthorityVO>
     */
    public List<SystemAuthorityVO> getList(SystemAuthorityQuery query) {

        SystemAuthority systemAuthority = BeanCopierUtil.copyProperties(query, SystemAuthority.class);
        LambdaQueryWrapper<SystemAuthority> queryWrapper = new LambdaQueryWrapper<>(systemAuthority);

        List<SystemAuthority> systemAuthorityList = list(queryWrapper);

        return BeanCopierUtil.copyToList(systemAuthorityList, SystemAuthorityVO.class);

    }

    /**
     * 获取系统-权限列表分页
     *
     * @param query SystemAuthorityQuery
     * @return Page<SystemAuthorityVO>
     */
    public Page<SystemAuthorityVO> getListPage(SystemAuthorityQuery query) {

        LambdaQueryWrapper<SystemAuthority> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(Objects.nonNull(query.getAuthorityId()), SystemAuthority::getAuthorityId, query.getAuthorityId())
                .like(StrUtil.isNotBlank(query.getAuthorityKey()), SystemAuthority::getAuthorityKey, query.getAuthorityKey())
                .like(StrUtil.isNotBlank(query.getMenuName()), SystemAuthority::getMenuName, query.getMenuName())
                .eq(Objects.nonNull(query.getDisabled()), SystemAuthority::getDisabled, query.getDisabled())

                .orderByDesc(SystemAuthority::getCreateTime)
                .orderByDesc(SystemAuthority::getAuthorityId)
        ;

        Page<SystemAuthority> page = page(new Page<>(query.getCurrent(), query.getSize()), queryWrapper);

        return (Page<SystemAuthorityVO>) page.convert(entity -> BeanCopierUtil.copyProperties(entity, SystemAuthorityVO.class));
    }

    /**
     * 系统-权限添加
     *
     * @param dto SystemAuthorityCreateDTO
     * @return SystemAuthorityVO
     */
    public SystemAuthorityVO createSystemAuthority(SystemAuthorityCreateDTO dto) {
        SystemAuthority systemAuthority = BeanCopierUtil.copyProperties(dto, SystemAuthority.class);
        systemAuthority.setAuthorityId(null);
        save(systemAuthority);
        return getDetail(systemAuthority.getAuthorityId());
    }

    /**
     * 系统-权限更新
     *
     * @param dto SystemAuthorityUpdateDTO
     * @return SystemAuthorityVO
     */
    public SystemAuthorityVO updateSystemAuthority(SystemAuthorityUpdateDTO dto) {
        Assert.notNull(dto.getAuthorityId(), "[authorityId]不可为空");
        SystemAuthority systemAuthority = BeanCopierUtil.copyProperties(dto, SystemAuthority.class);
        updateById(systemAuthority);
        return getDetail(systemAuthority.getAuthorityId());
    }


    /**
     * 系统-权限删除
     *
     * @param authorityId Long
     */
    public void deleteById(Long authorityId) {
        Assert.notNull(authorityId, "[authorityId]不可为空");
        removeById(authorityId);
    }


    /**
     * 系统-权限根据authorityId批量删除
     *
     * @param authorityIdList
     */
    public void deleteByIdList(List<Long> authorityIdList) {
        if (CollUtil.isEmpty(authorityIdList)) {
            return;
        }
        removeByIds(authorityIdList);
    }


    /**
     * 更新禁用状态
     *
     * @param authorityId Long
     */
    public void updateDisabled(Long authorityId) {

        Assert.notNull(authorityId, "[authorityId]不可为空");
        SystemAuthority systemAuthority = getById(authorityId);
        Assert.notNull(systemAuthority, "系统-权限信息不存在");

        Boolean disabled = Optional.ofNullable(systemAuthority.getDisabled()).orElse(false);
        SystemAuthority systemAuthorityUpdate = new SystemAuthority();
        systemAuthorityUpdate.setAuthorityId(authorityId);
        systemAuthorityUpdate.setDisabled(!disabled);
        updateById(systemAuthorityUpdate);

    }
}
