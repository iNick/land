package xin.nick.system.service;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import xin.nick.system.entity.SystemConfig;
import xin.nick.system.mapper.SystemConfigMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import xin.nick.system.domain.dto.SystemConfigUpdateDTO;
import xin.nick.system.domain.dto.SystemConfigCreateDTO;
import xin.nick.system.domain.query.SystemConfigQuery;
import xin.nick.system.domain.vo.SystemConfigVO;
import xin.nick.common.core.util.BeanCopierUtil;

import java.util.List;

/**
 * <p>
 * 系统配置 服务实现类
 * </p>
 *
 * @author Nick
 * @since 2023-11-28
 */
@Service
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class SystemConfigService extends ServiceImpl<SystemConfigMapper, SystemConfig> {


    /**
     * 获取系统配置详情
     * @param configId Long
     * @return SystemConfigVO
     */
    public SystemConfigVO getDetail(Long configId) {
        Assert.notNull(configId, "[configId]不可为空");
        SystemConfig systemConfig = getById(configId);
        Assert.notNull(systemConfig, "系统配置不存在");
        return BeanCopierUtil.copyProperties(systemConfig, SystemConfigVO.class);
    }

    /**
     * 获取系统配置列表
     * @param query SystemConfigQuery
     * @return List<SystemConfigVO>
     */
    public List<SystemConfigVO> getList(SystemConfigQuery query) {

        SystemConfig systemConfig = BeanCopierUtil.copyProperties(query, SystemConfig.class);
        LambdaQueryWrapper<SystemConfig> queryWrapper = new LambdaQueryWrapper<>(systemConfig);

        List<SystemConfig> systemConfigList = list(queryWrapper);

        return BeanCopierUtil.copyToList(systemConfigList, SystemConfigVO.class);

    }

    /**
     * 获取系统配置列表分页
     * @param query SystemConfigQuery
     * @return Page<SystemConfigVO>
     */
    public Page<SystemConfigVO> getListPage(SystemConfigQuery query) {

        SystemConfig systemConfig = BeanCopierUtil.copyProperties(query, SystemConfig.class);
        LambdaQueryWrapper<SystemConfig> queryWrapper = new LambdaQueryWrapper<>(systemConfig);

        Page<SystemConfig> page = page(new Page<>(query.getCurrent(), query.getSize()), queryWrapper);

        return (Page<SystemConfigVO>) page.convert(entity -> BeanCopierUtil.copyProperties(entity, SystemConfigVO.class));
    }

    /**
     * 系统配置添加
     * @param dto SystemConfigCreateDTO
     * @return SystemConfigVO
     */
    public SystemConfigVO createSystemConfig(SystemConfigCreateDTO dto) {
        SystemConfig systemConfig = BeanCopierUtil.copyProperties(dto, SystemConfig.class);
        systemConfig.setConfigId(null);
        save(systemConfig);
        return getDetail(systemConfig.getConfigId());
    }

    /**
     * 系统配置更新
     * @param dto SystemConfigUpdateDTO
     * @return SystemConfigVO
     */
    public SystemConfigVO updateSystemConfig(SystemConfigUpdateDTO dto) {
        Assert.notNull(dto.getConfigId(), "[configId]不可为空");
        SystemConfig systemConfig = BeanCopierUtil.copyProperties(dto, SystemConfig.class);
        updateById(systemConfig);
        return getDetail(systemConfig.getConfigId());
    }


    /**
     * 系统配置删除
     * @param configId Long
     */
    public void deleteById(Long configId) {
        Assert.notNull(configId, "[configId]不可为空");
        removeById(configId);
    }


    /**
     * 系统配置根据configId批量删除
     * @param configIdList
     */
    public void deleteByIdList(List<Long> configIdList) {
        if (CollUtil.isEmpty(configIdList)) {
            return;
        }
        removeByIds(configIdList);
    }


}
