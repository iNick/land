package xin.nick.system.service;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xin.nick.common.core.util.BeanCopierUtil;
import xin.nick.system.domain.dto.SystemRoleAuthorityCreateDTO;
import xin.nick.system.domain.dto.SystemRoleAuthorityUpdateDTO;
import xin.nick.system.domain.query.SystemRoleAuthorityQuery;
import xin.nick.system.domain.vo.SystemRoleAuthorityVO;
import xin.nick.system.entity.SystemRoleAuthority;
import xin.nick.system.mapper.SystemRoleAuthorityMapper;

import java.util.List;

/**
 * <p>
 * 系统-角色权限关联 服务实现类
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Service
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class SystemRoleAuthorityService extends ServiceImpl<SystemRoleAuthorityMapper, SystemRoleAuthority> {


    /**
     * 获取系统-角色权限关联详情
     *
     * @param id Long
     * @return SystemRoleAuthorityVO
     */
    public SystemRoleAuthorityVO getDetail(Long id) {
        Assert.notNull(id, "[id]不可为空");
        SystemRoleAuthority systemRoleAuthority = getById(id);
        Assert.notNull(systemRoleAuthority, "系统-角色权限关联不存在");
        return BeanCopierUtil.copyProperties(systemRoleAuthority, SystemRoleAuthorityVO.class);
    }

    /**
     * 获取系统-角色权限关联列表
     *
     * @param query SystemRoleAuthorityQuery
     * @return List<SystemRoleAuthorityVO>
     */
    public List<SystemRoleAuthorityVO> getList(SystemRoleAuthorityQuery query) {

        SystemRoleAuthority systemRoleAuthority = BeanCopierUtil.copyProperties(query, SystemRoleAuthority.class);
        LambdaQueryWrapper<SystemRoleAuthority> queryWrapper = new LambdaQueryWrapper<>(systemRoleAuthority);

        List<SystemRoleAuthority> systemRoleAuthorityList = list(queryWrapper);

        return BeanCopierUtil.copyToList(systemRoleAuthorityList, SystemRoleAuthorityVO.class);

    }

    /**
     * 获取系统-角色权限关联列表分页
     *
     * @param query SystemRoleAuthorityQuery
     * @return Page<SystemRoleAuthorityVO>
     */
    public Page<SystemRoleAuthorityVO> getListPage(SystemRoleAuthorityQuery query) {

        SystemRoleAuthority systemRoleAuthority = BeanCopierUtil.copyProperties(query, SystemRoleAuthority.class);
        LambdaQueryWrapper<SystemRoleAuthority> queryWrapper = new LambdaQueryWrapper<>(systemRoleAuthority);

        Page<SystemRoleAuthority> page = page(new Page<>(query.getCurrent(), query.getSize()), queryWrapper);

        return (Page<SystemRoleAuthorityVO>) page.convert(entity -> BeanCopierUtil.copyProperties(entity, SystemRoleAuthorityVO.class));
    }

    /**
     * 系统-角色权限关联添加
     *
     * @param dto SystemRoleAuthorityCreateDTO
     * @return SystemRoleAuthorityVO
     */
    public SystemRoleAuthorityVO createSystemRoleAuthority(SystemRoleAuthorityCreateDTO dto) {
        SystemRoleAuthority systemRoleAuthority = BeanCopierUtil.copyProperties(dto, SystemRoleAuthority.class);
        systemRoleAuthority.setId(null);
        save(systemRoleAuthority);
        return getDetail(systemRoleAuthority.getId());
    }

    /**
     * 系统-角色权限关联更新
     *
     * @param dto SystemRoleAuthorityUpdateDTO
     * @return SystemRoleAuthorityVO
     */
    public SystemRoleAuthorityVO updateSystemRoleAuthority(SystemRoleAuthorityUpdateDTO dto) {
        Assert.notNull(dto.getId(), "[id]不可为空");
        SystemRoleAuthority systemRoleAuthority = BeanCopierUtil.copyProperties(dto, SystemRoleAuthority.class);
        updateById(systemRoleAuthority);
        return getDetail(systemRoleAuthority.getId());
    }


    /**
     * 系统-角色权限关联删除
     *
     * @param id Long
     */
    public void deleteById(Long id) {
        Assert.notNull(id, "[id]不可为空");
        removeById(id);
    }


    /**
     * 系统-角色权限关联根据id批量删除
     *
     * @param idList
     */
    public void deleteByIdList(List<Long> idList) {
        if (CollUtil.isEmpty(idList)) {
            return;
        }
        removeByIds(idList);
    }


}
