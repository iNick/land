package xin.nick.system.service;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xin.nick.common.core.util.BeanCopierUtil;
import xin.nick.system.domain.dto.SystemUserRoleCreateDTO;
import xin.nick.system.domain.dto.SystemUserRoleUpdateDTO;
import xin.nick.system.domain.query.SystemUserRoleQuery;
import xin.nick.system.domain.vo.SystemUserRoleVO;
import xin.nick.system.entity.SystemUserRole;
import xin.nick.system.mapper.SystemUserRoleMapper;

import java.util.List;

/**
 * <p>
 * 系统-用户角色关联 服务实现类
 * </p>
 *
 * @author Nick
 * @since 2023-09-21
 */
@Service
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class SystemUserRoleService extends ServiceImpl<SystemUserRoleMapper, SystemUserRole> {


    /**
     * 获取系统-用户角色关联详情
     *
     * @param id Long
     * @return SystemUserRoleVO
     */
    public SystemUserRoleVO getDetail(Long id) {
        Assert.notNull(id, "[id]不可为空");
        SystemUserRole systemUserRole = getById(id);
        Assert.notNull(systemUserRole, "系统-用户角色关联不存在");
        return BeanCopierUtil.copyProperties(systemUserRole, SystemUserRoleVO.class);
    }

    /**
     * 获取系统-用户角色关联列表
     *
     * @param query SystemUserRoleQuery
     * @return List<SystemUserRoleVO>
     */
    public List<SystemUserRoleVO> getList(SystemUserRoleQuery query) {

        SystemUserRole systemUserRole = BeanCopierUtil.copyProperties(query, SystemUserRole.class);
        LambdaQueryWrapper<SystemUserRole> queryWrapper = new LambdaQueryWrapper<>(systemUserRole);

        List<SystemUserRole> systemUserRoleList = list(queryWrapper);

        return BeanCopierUtil.copyToList(systemUserRoleList, SystemUserRoleVO.class);

    }

    /**
     * 获取系统-用户角色关联列表分页
     *
     * @param query SystemUserRoleQuery
     * @return Page<SystemUserRoleVO>
     */
    public Page<SystemUserRoleVO> getListPage(SystemUserRoleQuery query) {

        SystemUserRole systemUserRole = BeanCopierUtil.copyProperties(query, SystemUserRole.class);
        LambdaQueryWrapper<SystemUserRole> queryWrapper = new LambdaQueryWrapper<>(systemUserRole);

        Page<SystemUserRole> page = page(new Page<>(query.getCurrent(), query.getSize()), queryWrapper);

        return (Page<SystemUserRoleVO>) page.convert(entity -> BeanCopierUtil.copyProperties(entity, SystemUserRoleVO.class));
    }

    /**
     * 系统-用户角色关联添加
     *
     * @param dto SystemUserRoleCreateDTO
     * @return SystemUserRoleVO
     */
    public SystemUserRoleVO createSystemUserRole(SystemUserRoleCreateDTO dto) {
        SystemUserRole systemUserRole = BeanCopierUtil.copyProperties(dto, SystemUserRole.class);
        systemUserRole.setId(null);
        save(systemUserRole);
        return getDetail(systemUserRole.getId());
    }

    /**
     * 系统-用户角色关联更新
     *
     * @param dto SystemUserRoleUpdateDTO
     * @return SystemUserRoleVO
     */
    public SystemUserRoleVO updateSystemUserRole(SystemUserRoleUpdateDTO dto) {
        Assert.notNull(dto.getId(), "[id]不可为空");
        SystemUserRole systemUserRole = BeanCopierUtil.copyProperties(dto, SystemUserRole.class);
        updateById(systemUserRole);
        return getDetail(systemUserRole.getId());
    }


    /**
     * 系统-用户角色关联删除
     *
     * @param id Long
     */
    public void deleteById(Long id) {
        Assert.notNull(id, "[id]不可为空");
        removeById(id);
    }


    /**
     * 系统-用户角色关联根据id批量删除
     *
     * @param idList
     */
    public void deleteByIdList(List<Long> idList) {
        if (CollUtil.isEmpty(idList)) {
            return;
        }
        removeByIds(idList);
    }


}
