package xin.nick.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import xin.nick.common.core.util.MyAssert;
import xin.nick.common.core.util.QueryUtil;
import xin.nick.common.core.util.ResultUtil;
import xin.nick.common.core.util.UserIdUtil;
import xin.nick.system.config.ApplicationSystemProperties;
import xin.nick.system.domain.dto.ChangePasswordDTO;
import xin.nick.system.domain.dto.SystemUserCreateDTO;
import xin.nick.system.domain.dto.SystemUserUpdateDTO;
import xin.nick.system.domain.query.SystemUserQuery;
import xin.nick.system.domain.vo.CurrentUserInfoVO;
import xin.nick.system.domain.vo.SystemRoleVO;
import xin.nick.system.domain.vo.SystemUserVO;
import xin.nick.system.entity.SystemRole;
import xin.nick.system.entity.SystemUser;
import xin.nick.system.manager.SystemRoleManager;
import xin.nick.system.manager.SystemUserManager;
import xin.nick.system.mapper.SystemUserMapper;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * <p>
 * 系统用户信息 服务实现类
 * </p>
 *
 * @author Nick
 * @since 2023-01-03
 */
@Service
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class SystemUserService extends ServiceImpl<SystemUserMapper, SystemUser> {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ApplicationSystemProperties systemProperties;
    private final SystemUserManager systemUserManager;
    private final SystemRoleManager roleManager;

    /**
     * 获取系统用户信息详情
     *
     * @param id Long
     * @return SystemUserVO
     */
    public SystemUserVO getDetail(Long id) {
        Assert.notNull(id, "[id]不可为空");
        SystemUser systemUser = getById(id);
        Assert.notNull(systemUser, "系统用户信息不存在");
        SystemUserVO systemUserVO = BeanUtil.copyProperties(systemUser, SystemUserVO.class);

        // 添加角色信息
        List<SystemRole> roleList = roleManager.getRoleListByUserId(id);
        List<SystemRoleVO> roleVOList = BeanUtil.copyToList(roleList, SystemRoleVO.class);
        systemUserVO.setRoleList(roleVOList);

        return systemUserVO;
    }

    /**
     * 获取系统用户信息列表
     *
     * @param query SystemUserQuery
     * @return List<SystemUserVO>
     */
    public List<SystemUserVO> getList(SystemUserQuery query) {

        SystemUser systemUser = BeanUtil.copyProperties(query, SystemUser.class);
        LambdaQueryWrapper<SystemUser> queryWrapper = new LambdaQueryWrapper<>(systemUser);

        List<SystemUser> systemUserList = list(queryWrapper);

        return BeanUtil.copyToList(systemUserList, SystemUserVO.class);

    }

    /**
     * 获取系统用户信息列表分页
     *
     * @param query SystemUserQuery
     * @return Page<SystemUserVO>
     */
    public Page<SystemUserVO> getListPage(SystemUserQuery query) {

        Long userId = query.getUserId();
        String username = query.getUsername();
        String nickname = query.getNickname();
        Boolean disabled = query.getDisabled();

        LambdaQueryWrapper<SystemUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(Objects.nonNull(userId), SystemUser::getUserId, userId)
                .like(StringUtils.hasLength(username), SystemUser::getUsername, username)
                .like(StringUtils.hasLength(nickname), SystemUser::getNickname, nickname)
                .eq(Objects.nonNull(disabled), SystemUser::getDisabled, disabled)
        ;

        queryWrapper.orderByDesc(SystemUser::getCreateTime);

        Page<SystemUser> page = page(QueryUtil.getQueryPage(), queryWrapper);

        return ResultUtil.convertPage(page, SystemUserVO.class);

    }

    /**
     * 系统用户信息添加
     *
     * @param dto SystemUserDTO
     * @return SystemUserVO
     */
    public SystemUserVO createSystemUser(SystemUserCreateDTO dto) {


        systemUserManager.checkUsernameRepeat(dto.getUsername(), null);

        SystemUser systemUser = BeanUtil.copyProperties(dto, SystemUser.class);
        systemUser.setUserId(null);
        systemUser.setPassword(bCryptPasswordEncoder.encode(systemUser.getPassword()));

        save(systemUser);

        // 更新角色关联
        roleManager.updateUserRoleList(systemUser.getUserId(), dto.getRoleIdList());

        return getDetail(systemUser.getUserId());

    }

    /**
     * 系统用户信息更新
     *
     * @param dto SystemUserUpdateDTO
     * @return SystemUserVO
     */
    public SystemUserVO updateSystemUser(SystemUserUpdateDTO dto) {

        Long userId = dto.getUserId();
        Assert.notNull(userId, "[userId]不可为空");
        systemUserManager.checkUsernameRepeat(dto.getUsername(), userId);

        SystemUser systemUser = BeanUtil.copyProperties(dto, SystemUser.class);
        String userPassword = systemUser.getPassword();
        if (StringUtils.hasLength(userPassword)) {
            userPassword = bCryptPasswordEncoder.encode(systemUser.getPassword());
            systemUser.setPassword(userPassword);
        }

        updateById(systemUser);

        // 更新角色关联
        roleManager.updateUserRoleList(systemUser.getUserId(), dto.getRoleIdList());

        return getDetail(systemUser.getUserId());

    }


    /**
     * 系统用户信息删除
     *
     * @param id Long
     */
    public void deleteById(Long id) {
        Assert.notNull(id, "[id]不可为空");

        Long currentUserId = UserIdUtil.getUserId();
        Assert.isTrue(!Objects.equals(currentUserId, id), "不可以删除自己");
        Assert.isTrue(!systemUserManager.isRoot(id), "不可以删除超级用户");

        removeById(id);
    }


    /**
     * 系统用户信息根据id批量删除
     *
     * @param idList List<Long>
     */
    public void deleteByIdList(List<Long> idList) {
        if (CollUtil.isEmpty(idList)) {
            return;
        }
        Long currentUserId = UserIdUtil.getUserId();
        for (Long id : idList) {
            Assert.isTrue(!Objects.equals(currentUserId, id), "不可以删除自己");
            Assert.isTrue(!systemUserManager.isRoot(id), "不可以删除超级用户");
        }

        removeByIds(idList);
    }

    /**
     * 重置指定用户密码
     *
     * @param id Long
     */
    public void resetPassword(Long id) {
        systemUserManager.checkUser(id);

        Assert.isTrue(!systemUserManager.isRoot(id), "不可以重置超级用户");

        SystemUser systemUserUpdate = new SystemUser();
        systemUserUpdate.setUserId(id);
        systemUserUpdate.setPassword(bCryptPasswordEncoder.encode(systemProperties.getDefaultPassword()));
        updateById(systemUserUpdate);
    }

    /**
     * 更新禁用状态
     *
     * @param id Long
     */
    public void updateDisabled(Long id) {
        Assert.notNull(id, "[id]不可为空");

        Long currentUserId = UserIdUtil.getUserId();
        Assert.isTrue(!Objects.equals(currentUserId, id), "不可以禁用自己");
        Assert.isTrue(!systemUserManager.isRoot(id), "不可以禁用超级用户");

        SystemUser systemUser = getById(id);
        Assert.notNull(systemUser, "系统用户不存在");

        Boolean disabled = Optional.ofNullable(systemUser.getDisabled()).orElse(false);
        SystemUser systemUserUpdate = new SystemUser();
        systemUserUpdate.setUserId(id);
        systemUserUpdate.setDisabled(!disabled);
        updateById(systemUserUpdate);
    }

    /**
     * 修改当前用户密码
     *
     * @param changePasswordDTO ChangePasswordDTO
     */
    public void changePassword(ChangePasswordDTO changePasswordDTO) {

        // 获取用户
        Long currentUserId = UserIdUtil.getUserId();
        SystemUser systemUser = getById(currentUserId);
        MyAssert.notNull(systemUser, "系统用户数据异常,请联系管理员");

        // 检查原密码
        String userPassword = systemUser.getPassword();
        boolean matches = bCryptPasswordEncoder.matches(changePasswordDTO.getPassword(), userPassword);
        org.springframework.util.Assert.isTrue(matches, "用户密码错误");

        // 设置新密码
        SystemUser systemUserUpdate = new SystemUser();
        systemUserUpdate.setUserId(currentUserId);
        systemUserUpdate.setPassword(bCryptPasswordEncoder.encode(changePasswordDTO.getNewPassword()));
        updateById(systemUserUpdate);


    }

    public CurrentUserInfoVO getCurrentUserInfo() {
        Long currentUserId = UserIdUtil.getUserId();
        SystemUser systemUser = getById(currentUserId);
        return BeanUtil.copyProperties(systemUser, CurrentUserInfoVO.class);
    }
}
