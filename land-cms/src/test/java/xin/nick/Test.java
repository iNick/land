package xin.nick;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Nick
 * @since 2023/11/23
 */
public class Test {


    public static void main(String[] args) {
        // 创建密码
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode("123456");
        System.out.println(encode);
    }


}
