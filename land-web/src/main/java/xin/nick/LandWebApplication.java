package xin.nick;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Nick
 * @since 2023/5/30
 */
@SpringBootApplication
@Slf4j
public class LandWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(LandWebApplication.class, args);
        log.info("LandWebApplication started!!!");
        log.info("Land管理系统-WEB启动!!!");
    }

}