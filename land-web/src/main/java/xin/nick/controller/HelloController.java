package xin.nick.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Nick
 * @since 2023/5/29
 */
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String sayHello(String message) {
        return "Hello " + message;
    }
}
