/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50742 (5.7.42-log)
 Source Host           : localhost:3306
 Source Schema         : project_db

 Target Server Type    : MySQL
 Target Server Version : 50742 (5.7.42-log)
 File Encoding         : 65001

 Date: 07/04/2024 18:29:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for system_authority
-- ----------------------------
DROP TABLE IF EXISTS `system_authority`;
CREATE TABLE `system_authority`  (
  `authority_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `authority_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '权限key',
  `authority_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '权限路径',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父级id',
  `menu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '菜单名字',
  `menu_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '菜单类型',
  `menu_meta` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '菜单元数据',
  `sort` int(10) NULL DEFAULT 1 COMMENT '排序(越小越靠前)',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `version` bigint(20) NULL DEFAULT 1 COMMENT '版本号',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `disabled` tinyint(1) NULL DEFAULT 0 COMMENT '是否禁用(1是,0否)',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '是否删除(1是,0否)',
  PRIMARY KEY (`authority_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统-权限' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_authority
-- ----------------------------
INSERT INTO `system_authority` VALUES (1, 'article:test:*', '/article/*', 0, '文章管理测试全部权限', 'BUTTON', '{}', 1, '测试权限菜单', 1, NULL, '2024-02-29 17:37:15', NULL, '2024-02-29 18:08:56', 0, 0);
INSERT INTO `system_authority` VALUES (2, 'article:test:no', '/article/sayNo', 0, NULL, 'BUTTON', '{}', 1, NULL, 1, NULL, '2024-02-29 17:37:15', NULL, '2024-02-29 18:08:50', 0, 0);
INSERT INTO `system_authority` VALUES (3, 'article:test:hello', '/article/sayHello', 0, NULL, 'BUTTON', '{}', 1, NULL, 1, NULL, '2024-02-29 18:07:05', NULL, '2024-02-29 18:08:52', 0, 0);
INSERT INTO `system_authority` VALUES (4, 'article:article-info:list', '/article/article-info/**', 0, '文章信息全部权限', 'BUTTON', '{}', 1, '文章信息全部权限', 1, NULL, '2024-03-21 16:54:08', NULL, '2024-03-21 17:28:27', 0, 0);
INSERT INTO `system_authority` VALUES (5, 'group:group-info:list', '/group/group-info/**', 0, NULL, NULL, NULL, 1, NULL, 1, NULL, '2024-03-21 17:01:35', NULL, '2024-03-21 17:28:02', 0, 0);

-- ----------------------------
-- Table structure for system_config
-- ----------------------------
DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config`  (
  `config_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '配置id',
  `config_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '配置名字',
  `config_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '配置key',
  `config_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '配置value',
  `config_json` json NULL COMMENT '配置json',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `version` bigint(20) NULL DEFAULT 1 COMMENT '版本号',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `disabled` tinyint(1) NULL DEFAULT 0 COMMENT '是否禁用(1是,0否)',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '是否删除(1是,0否)',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_config
-- ----------------------------

-- ----------------------------
-- Table structure for system_role
-- ----------------------------
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE `system_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色key',
  `role_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色名字',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `version` bigint(20) NULL DEFAULT 1 COMMENT '版本号',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `disabled` tinyint(1) NULL DEFAULT 0 COMMENT '是否禁用(1是,0否)',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '是否删除(1是,0否)',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统-角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_role
-- ----------------------------
INSERT INTO `system_role` VALUES (1, 'test', '测试角色', '测试', 1, NULL, '2024-02-27 10:26:31', NULL, '2024-02-27 10:26:33', 0, 0);

-- ----------------------------
-- Table structure for system_role_authority
-- ----------------------------
DROP TABLE IF EXISTS `system_role_authority`;
CREATE TABLE `system_role_authority`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色权限id',
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  `authority_id` bigint(20) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统-角色权限关联' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_role_authority
-- ----------------------------
INSERT INTO `system_role_authority` VALUES (1, 1, 1);
INSERT INTO `system_role_authority` VALUES (2, 1, 4);
INSERT INTO `system_role_authority` VALUES (3, 1, 2);

-- ----------------------------
-- Table structure for system_user
-- ----------------------------
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '系统用户id',
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '系统用户名字(账号)',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '系统用户密码',
  `nickname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '系统用户昵称',
  `tel_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '系统用户手机号',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `version` bigint(20) NULL DEFAULT 1 COMMENT '版本号',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `disabled` tinyint(1) NULL DEFAULT 0 COMMENT '是否禁用(1是,0否)',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '是否删除(1是,0否)',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统-用户' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_user
-- ----------------------------
INSERT INTO `system_user` VALUES (1, 'nick', '$2a$10$VTuWI98oghFDked5Ftnkke.N6TqBFQrUJOShwQRk4TWKyOj7os.vC', 'Nick', '13141234567', NULL, 1, NULL, '2022-11-27 17:54:36', NULL, '2023-01-04 10:54:10', 0, 0);
INSERT INTO `system_user` VALUES (2, 'test', '$2a$10$VTuWI98oghFDked5Ftnkke.N6TqBFQrUJOShwQRk4TWKyOj7os.vC', '测试', '13141234567', NULL, 1, NULL, '2022-11-27 17:54:36', NULL, '2023-01-04 10:54:12', 0, 0);
INSERT INTO `system_user` VALUES (3, 'test2', '$2a$10$VTuWI98oghFDked5Ftnkke.N6TqBFQrUJOShwQRk4TWKyOj7os.vC', '测试2', '13141234567', NULL, 1, NULL, '2024-03-21 16:07:37', NULL, '2024-03-21 16:07:51', 0, 0);
INSERT INTO `system_user` VALUES (4, 'test3', '$2a$10$VTuWI98oghFDked5Ftnkke.N6TqBFQrUJOShwQRk4TWKyOj7os.vC', '测试3', '13141234567', NULL, 1, NULL, '2024-03-21 16:07:37', NULL, '2024-03-21 16:07:51', 0, 0);

-- ----------------------------
-- Table structure for system_user_role
-- ----------------------------
DROP TABLE IF EXISTS `system_user_role`;
CREATE TABLE `system_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户角色id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统-用户角色关联' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_user_role
-- ----------------------------
INSERT INTO `system_user_role` VALUES (1, 2, 1);

SET FOREIGN_KEY_CHECKS = 1;
